//
//  AddressCell.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class AddressCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var defaultButton: UIButton!
    
    var editBlock : ((_ model : AddressInfo)->())?
    var deleteBlock : ((_ model : AddressInfo)->())?
    
    var model : AddressInfo?{
        didSet{
            guard let m = model else {
                return
            }
            nameLabel.text = m.name
            DistrictManager.shareInstance.getAddressByCode(m.province_id, m.city_id, m.district_id) { [weak self](str) in
                self?.addressLabel.text = "\(str) \(m.address ?? "")"//"\(m.address1 ?? "")\(m.address2 ?? "")"
            }
            
            defaultButton.isSelected = m.is_default
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    ///编辑按钮
    @IBAction func editButtonClick(_ sender: Any) {
        guard let e = editBlock else{
            return
        }
        guard let m = model else {
            return
        }
        e(m)
    }
    
    ///删除按钮
    @IBAction func deleteButtonClick(_ sender: Any) {
        guard let e = deleteBlock else{
            return
        }
        guard let m = model else {
            return
        }
        e(m)
    }
}
