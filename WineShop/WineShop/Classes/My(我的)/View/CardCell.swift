//
//  CardCell.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    
    var model : CardInfo?{
        didSet{
            guard let c = model else{
                return
            }
            bankNameLabel.text = c.bank_name
            cardNumberLabel.text = c.card_num
            typeLabel.text = c.card_type
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
