//
//  BuyHistoryCell.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class BuyHistoryCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var model : OrderInfo?{
        didSet{
            guard let o = model else{
                return
            }
            NetWorkRequestTool.downloadImageToImageView(iconImageView, o.img_url)
            nameLabel.text = o.name
            countLabel.text = "x\(o.amount)"
            totalLabel.text = "￥\(o.total_price)"
            dateLabel.text = o.create_time
            if let s = WineOrderStatus(rawValue: o.status){
                switch s {
                case WineOrderStatus.WineOrderStatusNonPayment:
                    statusLabel.text = "未支付"
                    break
                case WineOrderStatus.WineOrderStatusHavePaid:
                    statusLabel.text = "已支付"
                    break
                case WineOrderStatus.WineOrderStatusShipped:
                    statusLabel.text = "已发货"
                    break
                case WineOrderStatus.WineOrderStatusReceived:
                    statusLabel.text = "已收货"
                    break
                case WineOrderStatus.WineOrderStatusCancle:
                    statusLabel.text = "已取消"
                    break
                }
            }else{
                statusLabel.text = " "
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
