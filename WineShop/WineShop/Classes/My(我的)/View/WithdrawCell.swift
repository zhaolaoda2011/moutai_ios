//
//  WithdrawCell.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class WithdrawCell: UITableViewCell {
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    
    var model : WithdrawInfo?{
        didSet{
            guard let w = model else {
                return
            }
            numberLabel.text = "\(w.id)"
            dateLabel.text = w.create_time
            totalLabel.text = "￥\(w.bonus)"
            switch w.type {
            case 1:
                typeLabel.text = "一级成员"
                break
            case 2:
                typeLabel.text = "二级成员"
                break
            case 9:
                typeLabel.text = "合伙分红"
                break
            default:
                typeLabel.text = "未知"
                break
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        numberLabel.layer.cornerRadius = numberLabel.frame.width/2.0
        numberLabel.layer.masksToBounds = true
    }
}
