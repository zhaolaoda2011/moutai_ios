//
//  CityPickerView.swift
//  juzhuanwang
//
//  Created by apple on 2018/2/20.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class CityPickerView: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var pickerView: UIPickerView!
    // 地址数据
    var dataArray = [DistrictModel]()
    // 选中的index
    var selectProvinceIndex = 0
    var selectCityIndex = 0
    var selectDistrinctIndex = 0
    
    @IBOutlet weak var maskView: UIView!
    @IBOutlet weak var containerView: UIView!
    //确定按钮回调
    var submitBlock : ((_ province:DistrictModel,_ city:DistrictModel,_ distinct:DistrictModel)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @IBAction func cancelClick(_ sender: Any) {
        hide()
    }
    @IBAction func submitClick(_ sender: Any) {
        if submitBlock != nil{
            let province = dataArray[selectProvinceIndex]
            let city = province.child[selectCityIndex]
            let distinct = city.child[selectDistrinctIndex]
            
            self.submitBlock!(province,city,distinct)
            hide()
        }
    }
}

//MARK: picker view deleagte datasource
extension CityPickerView{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return dataArray.count
        case 1:
            let provice = dataArray[selectProvinceIndex]
            return provice.child.count
        case 2:
            let provice = dataArray[selectProvinceIndex]
            let distinct = provice.child[selectCityIndex]
            return distinct.child.count
        default:
            return 0
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = view as? UILabel
        if label == nil{
            label = UILabel()
            label?.font = kFontWithSize(size: 15)
            label?.textAlignment = NSTextAlignment.center
            label?.textColor = UIColor.withHex(hexInt: 0x333333)
        }
        switch component {
        case 0:
            let model = dataArray[row]
            label?.text = model.name
        case 1:
            let provice = dataArray[selectProvinceIndex]
            let model = provice.child[row]
            label?.text = model.name
        case 2:
            let provice = dataArray[selectProvinceIndex]
            let distinct = provice.child[selectCityIndex]
            let model = distinct.child[row]
            label?.text = model.name
        default:
            break
        }
        return label!
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch component {
        case 0:
            selectProvinceIndex = row
            selectCityIndex = 0
            selectDistrinctIndex = 0
            pickerView.reloadComponent(1)
            pickerView.reloadComponent(2)
            pickerView.selectRow(0, inComponent: 1, animated: true)
            pickerView.selectRow(0, inComponent: 2, animated: true)
        case 1:
            selectCityIndex = row
            selectDistrinctIndex = 0
            pickerView.reloadComponent(2)
            pickerView.selectRow(0, inComponent: 2, animated: true)
        case 2:
            selectDistrinctIndex = row
        default:
            break
        }
    }
}

// MARK: - 显示 隐藏
extension CityPickerView{
    func setupView(){
        UIApplication.shared.keyWindow?.endEditing(true)
        self.view.backgroundColor = UIColor.clear
        //containerView.layer.cornerRadius = 2
        containerView.transform = CGAffineTransform(translationX: 0, y: kScreenHeight()-containerView.frame.minY)
        let tap = UITapGestureRecognizer(target: self, action: #selector(hide))
        maskView.addGestureRecognizer(tap)
    }
    //显示
    func show(){
        UIApplication.shared.keyWindow?.addSubview(self.view)
        UIApplication.shared.keyWindow?.rootViewController?.addChildViewController(self)
        self.view.frame = (UIApplication.shared.keyWindow?.rootViewController?.view.frame)!;
        maskView.alpha = 0
        UIView.animate(withDuration: 0.5) {
            self.maskView.alpha = 1
            self.containerView.transform = CGAffineTransform.identity
        }
    }
    //隐藏
    @objc func hide(){
        UIView.animate(withDuration: 0.5, animations: {
            self.view.alpha = 0
            self.containerView.transform = CGAffineTransform(translationX: 0, y: kScreenHeight()-self.containerView.frame.minY)
        }) { (isfinish) in
            self .removeFromParentViewController()
            self.view.removeFromSuperview()
        }
    }
}


