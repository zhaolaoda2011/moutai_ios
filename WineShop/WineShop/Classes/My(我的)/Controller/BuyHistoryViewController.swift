//
//  BuyHistoryViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class BuyHistoryViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var orderArray : [OrderInfo] = [OrderInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupMJRefresh(tableView, headerHeight: 0, showRefreshHeader: true, showRefreshFooter: true)
        getOrderlist()
    }
    
    ///加载数据
    override func loadData(_ isRefresh: Bool) {
        super.loadData(isRefresh)
        if isRefresh{
            //            salesArray = [SaleCompany]()
            //            tableView.reloadData()
        }
        getOrderlist()
    }
}

//MARK: ui
extension BuyHistoryViewController{
    private func setupUI(){
        self.title = "购买历史"
        tableView.register(UINib(nibName: "BuyHistoryCell", bundle: nil), forCellReuseIdentifier: "BuyHistoryCell")
    }
}

//MARK: tableview delegate datasource
extension BuyHistoryViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return orderArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BuyHistoryCell") as! BuyHistoryCell
        cell.model = orderArray[indexPath.section]
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 134
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: 网络请求
extension BuyHistoryViewController{
    private func getOrderlist(){
        let params = [
            "pageSize":kPageSize,
            "page":page
        ]
        NetWorkRequestTool.request(url: orderlist, methodType: .get, parameters: params) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let pageModel = NetWorkRequestTool.getPageModel(result)else{
                    return
                }
                self?.page = pageModel.current_page
                self?.last_page = pageModel.last_page
                
                guard let list = [OrderInfo].deserialize(from: pageModel.data) else{
                    return
                }
                
                if self?.page == 1 {
                    self?.orderArray.removeAll()
                    self?.tableView.mj_header.isHidden = !(list.count>0)
                    self?.tableView.mj_footer.isHidden = !(list.count>0)
                }
                
                list.forEach({ (sale) in
                    self?.orderArray.append(sale!)
                })
                
                self?.tableView.reloadData()
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
            self?.tableView.mj_header.endRefreshing()
            self?.tableView.mj_footer.endRefreshing()
        }
    }
}

