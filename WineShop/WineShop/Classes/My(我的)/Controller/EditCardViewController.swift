//
//  EditCardViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/17.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class EditCardViewController: BaseViewController  {
    @IBOutlet weak var cancelBindButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var bankTypeLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    
    var model : CardInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationStyle(show: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigationStyle(show: false)
    }
    
    
    @IBAction func cancelBindButtonClick(_ sender: Any) {
        guard let m = model else{
            return
        }
        let param = ["id":m.id]
        NetWorkRequestTool.request(url: bank_cardremove, methodType: .post, parameters: param) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                showMessageHUD("解绑成功")
                self?.navigationController?.popViewController(animated: true)
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
}

extension EditCardViewController{
    private func setupUI(){
        self.title = "编辑银行卡"
        self.view.backgroundColor = UIColor.withHex(hexInt: 0xED6369)
//        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: cancelBindButton.frame.size)
//        cancelBindButton.setBackgroundImage(image, for: UIControlState.normal)
//        cancelBindButton.clipsToBounds = true
//
//        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
//        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
//        buttonContainView.layer.shadowOpacity = 0.5
//        buttonContainView.layer.cornerRadius = 4
        
        guard let m  = model else{
            return
        }
        
        bankNameLabel.text = m.bank_name
        bankTypeLabel.text = m.card_type
        cardNumberLabel.text = m.card_num
    }
    
    private func setupNavigationBar(){
        //添加后退按钮
        if((self.navigationController?.viewControllers.count)! > 1){
            let button = UIButton(imageName: "btn_back_white", highImageName: "btn_back_white")
            button.sizeToFit()
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
            button.addTarget(self, action: #selector(customerBackClick), for: UIControlEvents.touchUpInside)
        }
    }
    
    @objc func customerBackClick(){
        setupNavigationStyle(show: false)
        self.navigationController?.popViewController(animated: true)
    }
    
    ///设置导航栏颜色 true为蓝色  false为白色
    private func setupNavigationStyle(show:Bool){
        //导航栏背景颜色
        let bgColor = show ? UIColor.withHex(hexInt: 0xED6369) : UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.createImage(color: bgColor), for: .default)
        //导航栏线的颜色
        let lineColor = show ? UIColor.withHex(hexInt: 0xED6369) : UIColor.withHex(hexInt: 0xE8E8E8)
        self.navigationController?.navigationBar.shadowImage = UIImage.createImage(color: lineColor)
        //导航栏字体颜色
        let titleFont = kFontWithSize(size: 18)
        let titleColor = show ? UIColor.white : kFontColor
        let titleAttributeDict = [NSAttributedStringKey.foregroundColor : titleColor,NSAttributedStringKey.font : titleFont]
        self.navigationController?.navigationBar.titleTextAttributes = titleAttributeDict
        //状态栏颜色
        UIApplication.shared.statusBarStyle = show ? UIStatusBarStyle.lightContent : UIStatusBarStyle.default
    }
}
