//
//  ForgetPwdViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class ForgetPwdViewController: BaseViewController {
    @IBOutlet weak var submitButton: UIButton!
    
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var rePwdTextField: UITextField!
    var isGetCode : Bool = false
    var resetSuccessBlock : ((_ phone : String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //验证码
    @IBAction func getCodeButtonClick(_ sender: Any) {
        let button = sender as! UIButton
        
        var param = [String:String]()
        if let mobile = phoneTextField.text{
            if mobile.isEmpty(){
                showMessageHUD("请填写手机号")
                return
            }else{
                if !Validate.phoneNum(mobile).isRight{
                    showMessageHUD("请填写正确的手机号")
                    return
                }else{
                    param["mobile"] = mobile
                }
            }
        }
        
        param["type"] = "4"
        NetWorkRequestTool.request(url: accountVerifyCode, methodType: .post, parameters: param) { (result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                showMessageHUD("获取验证码成功,请注意查收")
                button.startSMSWithDuration(duration: 30)
                self.isGetCode = true
            }else{
                NetWorkRequestTool.showRequestMessage(result)
                
            }
        }
    }
    
    @IBAction func submitButtonClick(_ sender: Any) {
        var param = [String:String]()
        
        if let mobile = phoneTextField.text{
            if mobile.isEmpty(){
                showMessageHUD("请填写手机号")
                return
            }else{
                if !Validate.phoneNum(mobile).isRight{
                    showMessageHUD("请填写正确的手机号")
                    return
                }else{
                    param["mobile"] = mobile
                }
            }
        }
        
        if !isGetCode{
            showMessageHUD("请获取验证码")
            return
        }
        
        if let code = codeTextField.text{
            if code.isEmpty(){
                showMessageHUD("请填写验证码")
                return
            }else{
                param["verify_code"] = code
            }
        }
        
        if let pwd = pwdTextField.text{
            if pwd.isEmpty(){
                showMessageHUD("请输入密码")
                return
            }else{
                param["password"] = pwd
            }
        }
        
        if let confirm_password = rePwdTextField.text{
            if confirm_password.isEmpty(){
                showMessageHUD("请再次输入密码")
                return
            }else{
                param["confirm_password"] = confirm_password
            }
        }
        
        if pwdTextField.text != rePwdTextField.text{
            showMessageHUD("两次输入的密码不一致")
            return
        }
        
        
        showLoadingHUD("")
        NetWorkRequestTool.request(url: accountchangePassword, methodType: .put, parameters: param) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                showMessageHUD("提交成功")
                if let block = self?.resetSuccessBlock {
                    block(self?.phoneTextField.text ?? "")
                }
                self?.dismiss(animated: true, completion: {
                })
                
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
}

//MARK: UI
extension ForgetPwdViewController{
    private func setupUI(){
        self.title = "忘记密码"
        
        self.view.backgroundColor = UIColor.white
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: submitButton.frame.size)
        submitButton.setBackgroundImage(image, for: UIControlState.normal)
        submitButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
        
        //添加后退按钮
        let button = UIButton(imageName: "ic_back", highImageName: "ic_back")
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        button.addTarget(self, action: #selector(customerbackClick), for: UIControlEvents.touchUpInside)
    }
    
    @objc func customerbackClick(){
        self.dismiss(animated: true) {
            
        }
    }
}
