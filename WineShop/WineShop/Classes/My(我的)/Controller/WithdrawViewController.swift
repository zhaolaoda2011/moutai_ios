//
//  WithdrawViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class WithdrawViewController: BaseViewController {
    @IBOutlet weak var numberButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var moneyButton: UIButton!
    @IBOutlet weak var originButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var withdrawInfoArray = [WithdrawInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupMJRefresh(tableView, headerHeight: 0, showRefreshHeader: true, showRefreshFooter: true)
        getfxlist()
    }

    ///加载数据
    override func loadData(_ isRefresh: Bool) {
        super.loadData(isRefresh)
        if isRefresh{
            //            salesArray = [SaleCompany]()
            //            tableView.reloadData()
        }
        getfxlist()
    }
}


//MARK: ui
extension WithdrawViewController{
    private func setupUI(){
        self.title = "收益详情"
        self.view.backgroundColor = UIColor.white
        tableView.register(UINib(nibName: "WithdrawCell", bundle: nil), forCellReuseIdentifier: "WithdrawCell")
    }
}

//MARK: tableview delegate datasource
extension WithdrawViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return withdrawInfoArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WithdrawCell") as! WithdrawCell
        cell.model = withdrawInfoArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension WithdrawViewController{
    private func getfxlist(){
        NetWorkRequestTool.request(url: fxearnList, methodType: .get, parameters: [:]) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let pageModel = NetWorkRequestTool.getPageModel(result)else{
                    return
                }
                self?.page = pageModel.current_page
                self?.last_page = pageModel.last_page
                
                guard let list = [WithdrawInfo].deserialize(from: pageModel.data) else{
                    return
                }
                
                if self?.page == 1 {
                    self?.withdrawInfoArray.removeAll()
                    self?.tableView.mj_header.isHidden = !(list.count>0)
                    self?.tableView.mj_footer.isHidden = !(list.count>0)
                }
                
                list.forEach({ (sale) in
                    self?.withdrawInfoArray.append(sale!)
                })
                
                self?.tableView.reloadData()
                
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
            self?.tableView.mj_header.endRefreshing()
            self?.tableView.mj_footer.endRefreshing()
        }
    }
}

