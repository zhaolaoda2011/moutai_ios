//
//  AddressManagerViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class AddressManagerViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    var addressList : [AddressInfo] = [AddressInfo]()
    var selectAddressBlock : ((_ address : AddressInfo)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAddressList()
    }
    
    //添加地址
    @IBAction func addAddressButtonClick(_ sender: Any) {
        let add = AddAddressViewController()
        self.navigationController?.pushViewController(add, animated: true)
    }
}

//MARK: ui
extension AddressManagerViewController{
    private func setupUI(){
        self.title = "地址管理"
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: addButton.frame.size)
        addButton.setBackgroundImage(image, for: UIControlState.normal)
        addButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
        
        tableView.register(UINib(nibName: "AddressCell", bundle: nil), forCellReuseIdentifier: "AddressCell")
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0)
    }
}

//MARK: tableview delegate datasource
extension AddressManagerViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return addressList.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell") as! AddressCell
        cell.model = addressList[indexPath.section]
        cell.editBlock = {[weak self] m in
            let edit = AddAddressViewController()
            edit.isModify = true
            edit.model = (self?.addressList[indexPath.section])!
            self?.navigationController?.pushViewController(edit, animated: true)
        }
        cell.deleteBlock = {[weak self] m in
            let alertController = UIAlertController(title: "提示",
                                                    message: "您确定要删除吗?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
            let okAction = UIAlertAction(title: "确定", style: .default, handler: {
                action in
                let param = ["id" : "\( m.id)"]
                showLoadingHUD("正在删除")
                NetWorkRequestTool.request(url: addressremove, methodType: .post, parameters: param, finished: { (result, error) in
                    hideAllHUD()
                    if NetWorkRequestTool.validateRequestRespone(result){
                        self?.getAddressList()
                    }else{
                        NetWorkRequestTool.showRequestMessage(result)
                    }
                })
            })
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self?.present(alertController, animated: true, completion: nil)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let block = selectAddressBlock else {
            return
        }
        let model = addressList[indexPath.section]
        block(model)
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: 网络请求
extension AddressManagerViewController{
    private func getAddressList(){
        NetWorkRequestTool.request(url: addresslist, methodType: .get, parameters: [:]) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [Any] else{
                    return
                }
                guard let list = [AddressInfo].deserialize(from: data) else{
                    return
                }
                self?.addressList.removeAll()
                list.forEach({ (model) in
                    self?.addressList.append(model!)
                })
                self?.tableView.reloadData()
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
}
