//
//  MyViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class MyViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var levelTitleLabel: UILabel!
    
    var titleArray = ["绑卡","地址管理","购买历史","收益"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupUI()
        getInfo()
    }
    
    //点击了昵称和手机号码
    @IBAction func nameLabelClick(_ sender: Any) {
        if !UserInfoTool.checkUserLogin(){
            let login = SignInViewController()
            self.present(login, animated: true) {
                
            }
            return
        }else{
            let info = MyInfoViewController()
            self.navigationController?.pushViewController(info, animated: true)
        }
    }
    
    
    ///点击了头像
    @IBAction func iconImageVIewClick(_ sender: Any) {
        if !UserInfoTool.checkUserLogin(){
            let login = SignInViewController()
            self.present(login, animated: true) {
                
            }
            return
        }else{
            let info = MyInfoViewController()
            self.navigationController?.pushViewController(info, animated: true)
        }
    }
    
    ///退出登录
    @IBAction func logOutButtonClick(_ sender: Any) {
        logout()
        
    }
}

//MARK: ui
extension MyViewController{
    private func setupUI(){
        iconImageView.layer.cornerRadius = iconImageView.frame.width/2.0
        iconImageView.layer.masksToBounds = true
        tableView.register(UINib(nibName: "MyCell", bundle: nil), forCellReuseIdentifier: "MyCell")
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: logOutButton.frame.size)
        logOutButton.setBackgroundImage(image, for: UIControlState.normal)
        logOutButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
        
        logOutButton.isHidden = !UserInfoTool.checkUserLogin()
        buttonContainView.isHidden = !UserInfoTool.checkUserLogin()
        
        if UserInfoTool.checkUserLogin(){
            let info = UserInfoTool.loadUserInfo()
            NetWorkRequestTool.downloadImageToImageView(iconImageView, info.avatar_url ?? "")
            nameLabel.text = info.username
            mobileLabel.text = info.mobile
            levelLabel.text = SystemConfigManager.shareInstance.systemConfigModel.fx_role[safe:info.fx_role]
        }else{
            nameLabel.text = "未登录"
            mobileLabel.text = "点击登录"
            levelLabel.text = nil
            iconImageView.image = UIImage(named: "ic_man_moren")
        }
        
        levelTitleLabel.text = SystemConfigManager.shareInstance.systemConfigModel.is_prod ? "级别" : "欢迎使用茅台黔坤!"
        levelLabel.isHidden = !SystemConfigManager.shareInstance.systemConfigModel.is_prod
        
        titleArray = SystemConfigManager.shareInstance.systemConfigModel.is_prod ? ["绑卡","地址管理","购买历史","收益"]:["地址管理","购买历史"]
        tableView.reloadData()
    }
}

//MARK: tableview delegate datasource
extension MyViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell") as! MyCell
        cell.nameLabel.text = titleArray[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if !UserInfoTool.checkUserLogin(){
            let login = SignInViewController()
            self.present(login, animated: true) {
                
            }
            return
        }
        if SystemConfigManager.shareInstance.systemConfigModel.is_prod{
            switch indexPath.row {
            case 0:
                let card = MyCardViewController()
                self.navigationController?.pushViewController(card, animated: true)
                break
            case 1:
                let address = AddressManagerViewController()
                self.navigationController?.pushViewController(address, animated: true)
                break
            case 2:
                let buy = BuyHistoryViewController()
                self.navigationController?.pushViewController(buy, animated: true)
                break
            case 3:
                let ear = EarningsViewController()
                self.navigationController?.pushViewController(ear, animated: true)
                break
            default:
                break
            }
        }else{
            switch indexPath.row {
            case 0:
                let card = AddressManagerViewController()
                self.navigationController?.pushViewController(card, animated: true)
                break
            case 1:
                let address = BuyHistoryViewController()
                self.navigationController?.pushViewController(address, animated: true)
                break
            default:
                break
            }
        }
    }
}

//MARK: 网络请求
extension MyViewController{
    ///退出登录
    func logout(){
        showLoadingHUD("正在退出登录")
        NetWorkRequestTool.request(url: accountLogout, methodType: .post, parameters: [:]) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                UserInfoTool.clearUserInfo()
                self?.setupUI()
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
    ///获取个人信息
    func getInfo(){
        if !UserInfoTool.checkUserLogin(){
            return
        }
        NetWorkRequestTool.request(url: accountProfile, methodType: .get, parameters: [:]) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let info = UserInfo.deserialize(from: data) else{
                    return
                }
                let io = UserInfoTool.loadUserInfo()
                info.token = io.token
                info.address = io.address
                UserInfoTool.saveUserInfo(user: info)
                self?.setupUI()
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
}
