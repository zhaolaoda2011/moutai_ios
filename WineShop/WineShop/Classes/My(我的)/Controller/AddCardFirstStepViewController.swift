//
//  AddCardFirstStepViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit
import HandyJSON

struct SaveCardParam : HandyJSON {
    var card_num : String?
    var account_name : String?
    var id_card : String?
    var mobile : String?
    var verify_code : String?
}

class AddCardFirstStepViewController: BaseViewController {
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var idCardTextField: UITextField!
    @IBOutlet weak var carNumTextField: UITextField!
    
    var param = SaveCardParam()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    
    @IBAction func nextButtonClick(_ sender: Any) {
        if let account_name = nameTextField.text{
            if account_name.isEmpty{
                showMessageHUD("请填写持卡人")
                return
            }else{
                param.account_name = account_name
            }
        }
        
        if let id_card = idCardTextField.text{
            if id_card.isEmpty{
                showMessageHUD("请填写持卡人证件号")
                return
            }else{
                param.id_card = id_card
            }
        }
        
        if let card_num = carNumTextField.text{
            if card_num.isEmpty{
                showMessageHUD("请填写卡号")
                return
            }else{
                param.card_num = card_num
            }
        }
        
        let second = AddCardSecondStepViewController()
        second.param = param
        self.navigationController?.pushViewController(second, animated: true)
    }
}

extension AddCardFirstStepViewController{
    private func setupUI(){
        self.title = "添加银行卡"
        self.view.backgroundColor = UIColor.white
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: nextButton.frame.size)
        nextButton.setBackgroundImage(image, for: UIControlState.normal)
        nextButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
    }
}
