//
//  EarningsViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class EarningsViewController: BaseViewController {
    @IBOutlet weak var withdrawButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var firstEarnLabel: UILabel!
    @IBOutlet weak var secondEarnLabel: UILabel!
    @IBOutlet weak var totalEarnLabel: UILabel!
    
    var cardModel : CardInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNavigationBar()
        getfx()
        getWithdrawDepositInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationStyle(show: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigationStyle(show: false)
    }

    //查看收益详情
    @IBAction func viewDetailButtonClick(_ sender: Any) {
        let withdraw = WithdrawViewController()
        self.navigationController?.pushViewController(withdraw, animated: true)
    }
    
    //提现
    @IBAction func withdrawButtonClick(_ sender: Any) {
        if let m = cardModel{
            if m.id < 0{
                showMessageHUD("请前往个人中心绑卡")
            }else{
                let getmoney = GetMoneyViewController()
                getmoney.cardModel = m
                self.navigationController?.pushViewController(getmoney, animated: true)
            }
        }else{
            showMessageHUD("请前往个人中心绑卡")
        }
    }
}

//MARK: ui
extension EarningsViewController{
    private func setupUI(){
        self.title = "收益"
        self.view.backgroundColor = UIColor.white
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: withdrawButton.frame.size)
        withdrawButton.setBackgroundImage(image, for: UIControlState.normal)
        withdrawButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
    }
    
    private func setupNavigationBar(){
        //添加后退按钮
        if((self.navigationController?.viewControllers.count)! > 1){
            let button = UIButton(imageName: "btn_back_white", highImageName: "btn_back_white")
            button.sizeToFit()
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
            button.addTarget(self, action: #selector(customerBackClick), for: UIControlEvents.touchUpInside)
        }
    }
    
    @objc func customerBackClick(){
        setupNavigationStyle(show: false)
        self.navigationController?.popViewController(animated: true)
    }
    
    ///设置导航栏颜色 true为蓝色  false为白色
    private func setupNavigationStyle(show:Bool){
        //导航栏背景颜色
        let bgColor = show ? UIColor.withHex(hexInt: 0xED6469) : UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.createImage(color: bgColor), for: .default)
        //导航栏线的颜色
        let lineColor = show ? UIColor.withHex(hexInt: 0xED6469) : UIColor.withHex(hexInt: 0xE8E8E8)
        self.navigationController?.navigationBar.shadowImage = UIImage.createImage(color: lineColor)
        //导航栏字体颜色
        let titleFont = kFontWithSize(size: 18)
        let titleColor = show ? UIColor.white : kFontColor
        let titleAttributeDict = [NSAttributedStringKey.foregroundColor : titleColor,NSAttributedStringKey.font : titleFont]
        self.navigationController?.navigationBar.titleTextAttributes = titleAttributeDict
        //状态栏颜色
        UIApplication.shared.statusBarStyle = show ? UIStatusBarStyle.lightContent : UIStatusBarStyle.default
    }
}

//MARK: 网络请求
extension EarningsViewController{
    private func getfx(){
        NetWorkRequestTool.request(url: fxinfo, methodType: .get, parameters: [:]) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let info = FxInfo.deserialize(from: data) else{
                    return
                }
                self?.firstEarnLabel.text = "￥\(info.first_earn)"
                self?.secondEarnLabel.text = "￥\(info.second_earn)"
                self?.totalEarnLabel.text = "总计收益: ￥\(info.first_earn+info.second_earn)元"
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
    //获取个人银行卡信息(包括默认银行卡&账户可用于提现的余额)
    private func getWithdrawDepositInfo(){
        showLoadingHUD("")
        NetWorkRequestTool.request(url: accountWithdrawDepositInfo, methodType: .get, parameters: [:]) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let info = CardInfo.deserialize(from: data) else{
                    return
                }
                self?.cardModel = info
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
}

