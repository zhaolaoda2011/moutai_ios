//
//  MyCardViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class MyCardViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    
    var cardArray = [CardInfo]()
    
    var isSelectMode : Bool = false
    var selectBlock : ((_ model : CardInfo)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationStyle(show: true)
        getCardList()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigationStyle(show: false)
    }
    
    //编辑银行卡
    @IBAction func editButtonClick(_ sender: Any) {
        let first = AddCardFirstStepViewController()
        self.navigationController?.pushViewController(first, animated: true)
    }
}

//MARK: ui
extension MyCardViewController{
    private func setupUI(){
        self.title = isSelectMode ? "选择银行卡" : "银行卡管理"
        self.view.backgroundColor = UIColor.black
        
        tableView.register(UINib(nibName: "CardCell", bundle: nil), forCellReuseIdentifier: "CardCell")
        tableView.contentInset = UIEdgeInsetsMake(0, 0, 120, 0)
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: editButton.frame.size)
        editButton.setBackgroundImage(image, for: UIControlState.normal)
        editButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 1 
        buttonContainView.layer.cornerRadius = 4
        
        editButton.isHidden = isSelectMode
        buttonContainView.isHidden = isSelectMode
    }
    
    private func setupNavigationBar(){
        //添加后退按钮
        if((self.navigationController?.viewControllers.count)! > 1){
            let button = UIButton(imageName: "btn_back_white", highImageName: "btn_back_white")
            button.sizeToFit()
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
            button.addTarget(self, action: #selector(customerBackClick), for: UIControlEvents.touchUpInside)
        }
    }
    
    @objc func customerBackClick(){
        setupNavigationStyle(show: false)
        self.navigationController?.popViewController(animated: true)
    }
    
    ///设置导航栏颜色 true为蓝色  false为白色
    private func setupNavigationStyle(show:Bool){
        //导航栏背景颜色
        let bgColor = show ? UIColor.withHex(hexInt: 0x2E3036) : UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.createImage(color: bgColor), for: .default)
        //导航栏线的颜色
        let lineColor = show ? UIColor.withHex(hexInt: 0x2E3036) : UIColor.withHex(hexInt: 0xE8E8E8)
        self.navigationController?.navigationBar.shadowImage = UIImage.createImage(color: lineColor)
        //导航栏字体颜色
        let titleFont = kFontWithSize(size: 18)
        let titleColor = show ? UIColor.white : kFontColor
        let titleAttributeDict = [NSAttributedStringKey.foregroundColor : titleColor,NSAttributedStringKey.font : titleFont]
        self.navigationController?.navigationBar.titleTextAttributes = titleAttributeDict
        //状态栏颜色
        UIApplication.shared.statusBarStyle = show ? UIStatusBarStyle.lightContent : UIStatusBarStyle.default
    }
}

//MARK: tableview delegate datasource
extension MyCardViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return cardArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell") as! CardCell
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.model = cardArray[indexPath.section]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = cardArray[indexPath.section]
        if isSelectMode{
            guard let block = selectBlock else{
                return
            }
            block(model)
            self.navigationController?.popViewController(animated: true)
        }else{
            let edit = EditCardViewController()
            edit.model = model
            self.navigationController?.pushViewController(edit, animated: true)
        }
    }
}

//MARK: 网络请求
extension MyCardViewController{
    func getCardList(){
        NetWorkRequestTool.request(url: bank_cardList, methodType: .get, parameters: [:]) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [Any] else{
                    return
                }
                guard let cards = [CardInfo].deserialize(from: data)else{
                    return
                }
                self?.cardArray.removeAll()
                cards.forEach({ (card) in
                    guard let c = card else{
                        return
                    }
                    self?.cardArray.append(c)
                })
                self?.tableView.reloadData()
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
}

