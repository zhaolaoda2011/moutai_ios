//
//  GetMoneyViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class GetMoneyViewController: BaseViewController {
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    
    @IBOutlet weak var selectCardButton: UIButton!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var totalLabel: UILabel!
    
    var cardModel : CardInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getWithdrawDepositInfo()
    }
    
    ///全部体现
    @IBAction func allButtonClick(_ sender: Any) {
        if let m = cardModel{
            priceTextField.text = m.allow_fee
        }
    }
    
    ///选择银行卡
    @IBAction func selectCardButtonClick(_ sender: Any) {
        let my = MyCardViewController()
        my.isSelectMode = true
        my.selectBlock = {[weak self]model in
            if let m = self?.cardModel{
                var nm = model
                nm.allow_fee = m.allow_fee
                self?.cardModel = nm
                self?.setupUI()
            }
        }
        self.navigationController?.pushViewController(my, animated: true)
    }
    
    ///提现
    @IBAction func getMoneyButtonClick(_ sender: Any) {
        if let price = priceTextField.text{
            if price.isEmpty{
                showMessageHUD("请填写提现金额")
                return
            }else{
                if !ValidateTool.isPurnFloat(price){
                    showMessageHUD("请填写正确的提现金额")
                    return
                }
                if let total_fee = Float(price){
                    if total_fee <= 0.0{
                        showMessageHUD("请填写正确的提现金额")
                        return
                    }
                }
                if let m = cardModel{
                    showLoadingHUD("")
                    let parma = ["bc_id":"\(m.id)","total_fee":price]
                    NetWorkRequestTool.request(url: bankwithdrawDeposit, methodType: .post, parameters: parma) { (result, error) in
                        hideAllHUD()
                        if NetWorkRequestTool.validateRequestRespone(result){
                            showMessageHUD("已提交申请")
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            NetWorkRequestTool.showRequestMessage(result)
                        }
                    }
                }else{
                    showMessageHUD("请选择银行卡")
                }
            }
        }
    }
}

//MARK: ui
extension GetMoneyViewController{
    private func setupUI(){
        self.title = "提现"
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: submitButton.frame.size)
        submitButton.setBackgroundImage(image, for: UIControlState.normal)
        submitButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
        
        if let m = cardModel{
            selectCardButton.setTitle("\(m.bank_name)\(m.card_type)(\(m.card_num))", for: UIControlState.normal)
            totalLabel.text = "您总共可体现\(m.allow_fee)元,"
        }
    }
}

extension GetMoneyViewController{
    //获取个人银行卡信息(包括默认银行卡&账户可用于提现的余额)
    private func getWithdrawDepositInfo(){
        NetWorkRequestTool.request(url: accountWithdrawDepositInfo, methodType: .get, parameters: [:]) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let info = CardInfo.deserialize(from: data) else{
                    return
                }
                self?.cardModel = info
                self?.setupUI()
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
}
