//
//  MyInfoViewController.swift
//  juzhuanwang
//
//  Created by zj on 2018/3/6.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit


class MyInfoViewController: BaseViewController {
    
    // 选择照片的方式：相册、相机
    private enum SeletPhotoMethod {
        case SeletPhotoMethodWithAlbum
        case SeletPhotoMethodWithCamera
    }
    @IBOutlet weak var avaterIcon: UIImageView! // 头像
    @IBOutlet weak var usernameTF: UITextField! // 用户名
    @IBOutlet weak var buttonContainView: UIView!
    
    var userID : Int = -1
    var imageKey : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "修改信息"
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewDidAppear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    
    // 保存
    @IBAction func saveBtnEvent(_ sender: Any) {
        self.view.endEditing(true)
        saveInforRequest()
    }
    
    // 上传头像
    @IBAction func updateAvaterEvent(_ sender: Any) {
        self.view.endEditing(true)
        alterSheetShow()
    }
    
}


// MARK: - UI
extension MyInfoViewController {
    func updateUI() {
        let info = UserInfoTool.loadUserInfo()
        NetWorkRequestTool.downloadImageToImageView(avaterIcon, info.avatar_url!)
        
        self.usernameTF.text = info.username
        
        self.view.backgroundColor = UIColor.white
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
    }
}

// MARK: - 选择图片

extension MyInfoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    private  func alterSheetShow() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet )
        
        let photoAction = UIAlertAction(title: "相册", style: .default) { (action) in
            
            self.photoLib(selectMethod: SeletPhotoMethod.SeletPhotoMethodWithAlbum)
        }
        alertController.addAction(photoAction)
        
        let cameraAction = UIAlertAction(title: "相机", style: .default) { (action) in
            self.photoLib(selectMethod: SeletPhotoMethod.SeletPhotoMethodWithCamera)
            
        }
        alertController.addAction(cameraAction)
        
        let cancelAction = UIAlertAction(title: "取消", style: .cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func photoLib(selectMethod: SeletPhotoMethod){
        //判断设置是否支持图片库
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            //初始化图片控制器
            let picker = UIImagePickerController()
            //设置代理
            picker.delegate = self
            //指定图片控制器类型
            picker.sourceType = selectMethod == .SeletPhotoMethodWithAlbum ? UIImagePickerControllerSourceType.photoLibrary : UIImagePickerControllerSourceType.camera
            //弹出控制器，显示界面
            self.present(picker, animated: true, completion: {
                () -> Void in
            })
        }else{
            //            print("读取相册错误")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [String : Any]) {
        //获取选取后的图片
        let pickedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        showLoadingHUD("")
        //上传
        self.uploadImage(image: pickedImage)
        //图片控制器退出
        self.dismiss(animated: true, completion:nil)
    }
    
}


// MARK: - 网络请求
extension MyInfoViewController {
    // 上传图片
    func uploadImage(image: UIImage){
        NetWorkRequestTool.uploadImage(image: image, uploadURL:"\(imgUpload)?type=apply") { [weak self] (result, err) in
            hideAllHUD()
            if let error = err {
                showMessageHUD("上传图片失败:\(error.localizedDescription)")
            }else {
                if NetWorkRequestTool.validateRequestRespone(result) {
                    let data = result!["data"] as? [String: Any]
                    //let full_url = data!["full_url"] as! String
                    guard let key =  data!["key"] as? String else{
                        return
                    }
                    //                    guard let full_url = data!["full_url"] as? String else {return}
                    self?.imageKey = key
                    //                    self?.avatar_url_full = full_url
                    self?.avaterIcon.image = image
                    
                }else {
                    NetWorkRequestTool.showRequestMessage(result)
                }
                
            }
        }
    }
    
    // 保存
    func saveInforRequest() {
        showLoadingHUD("")
        if let name = usernameTF.text{
            if name.isEmpty{
                showMessageHUD("请输入用户名")
                return
            }
        }
        
        if let key = imageKey{
            if key.isEmpty{
                showMessageHUD("图片key有问题")
                return
            }
            
        }
        
        let param : [String : Any] = [
            "username" : usernameTF.text ?? "",
            "avatar_url" : ((imageKey ?? "") as NSString).replacingOccurrences(of: "\\", with: "/")
        ]
        NetWorkRequestTool.request(url: accountupdate, methodType: .put, parameters: param) { (result, error) in
            hideAllHUD()
            if let err = error {
                showMessageHUD(err.localizedDescription)
            }else{
                if NetWorkRequestTool.validateRequestRespone(result) {
                    showMessageHUD("保存成功")
                    self.getInfo(io: UserInfoTool.loadUserInfo())
                }else {
                    NetWorkRequestTool.showRequestMessage(result)
                }
            }
        }
    }
    
    //获取个人信息
    private func getInfo(io : UserInfo){
        showLoadingHUD("初始化个人信息")
        NetWorkRequestTool.request(url: accountProfile, methodType: .get, parameters: [:]) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let info = UserInfo.deserialize(from: data) else{
                    return
                }
                info.token = io.token
                info.address = io.address
                UserInfoTool.saveUserInfo(user: info)
                self?.dismiss(animated: true) {
                    
                }
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
        
    }
}

