//
//  AddCardSecondStepViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class AddCardSecondStepViewController: BaseViewController {
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var serviceButton: UIButton!
    var isGetCode : Bool = false
    
    var param = SaveCardParam()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    @IBAction func serviceButtonClick(_ sender: Any) {
        let button = sender as! UIButton
        button.isSelected = !button.isSelected
    }
    
    ///获取验证码
    @IBAction func getCodeButtonClick(_ sender: Any) {
        if let mobile = phoneTextField.text{
            if mobile.isEmpty{
                showMessageHUD("请填写手机号")
                return
            }else{
                if !Validate.phoneNum(mobile).isRight{
                    showMessageHUD("请填写正确的手机号")
                    return
                }else{
                    let button = sender as! UIButton
                    
                    showLoadingHUD("")
                    let param = ["type":"3","mobile":mobile]
                    NetWorkRequestTool.request(url: accountVerifyCode, methodType: .post, parameters: param) { [weak self](result, error) in
                        hideAllHUD()
                        if NetWorkRequestTool.validateRequestRespone(result){
                            self?.isGetCode = true
                            button.startSMSWithDuration(duration: 30)
                            showMessageHUD("获取验证码成功,请注意查收")
                        }else{
                            NetWorkRequestTool.showRequestMessage(result)
                        }
                    }
                }
            }
        }
        
        
    }
    
    @IBAction func submitButtonClick(_ sender: Any) {
        
        if let mobile = phoneTextField.text{
            if mobile.isEmpty{
                showMessageHUD("请填写手机号")
                return
            }else{
                if Validate.phoneNum(mobile).isRight{
                    param.mobile = mobile
                }else{
                    showMessageHUD("请填写正确的手机号")
                    return
                }
            }
        }
        
        if !isGetCode{
            showMessageHUD("请获取验证码")
            return
        }
        
        if let verify_code = codeTextField.text{
            if verify_code.isEmpty{
                showMessageHUD("请填写验证码")
                return
            }else{
                param.verify_code = verify_code
            }
        }
        
        if !serviceButton.isSelected{
            showMessageHUD("请勾选同意服务协议")
            return
        }
        
        showLoadingHUD("正在提交")
        NetWorkRequestTool.request(url: bank_cardsave, methodType: .post, parameters: param.toJSON()) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                showMessageHUD("添加成功")
                self?.navigationController?.popToRootViewController(animated: true)
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
}

extension AddCardSecondStepViewController{
    private func setupUI(){
        self.title = "添加银行卡"
        self.view.backgroundColor = UIColor.white
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: submitButton.frame.size)
        submitButton.setBackgroundImage(image, for: UIControlState.normal)
        submitButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
    }
}
