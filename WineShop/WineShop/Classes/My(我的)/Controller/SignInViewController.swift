//
//  SignInViewController.swift
//  juzhuanwang
//
//  Created by zc on 2018/1/18.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class SignInViewController: BaseViewController {
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //关闭
    @IBAction func closeButtonClick(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    
    //忘记密码
    @IBAction func forgetPWDButton(_ sender: Any) {
        let sign = ForgetPwdViewController()
        sign.resetSuccessBlock = { [weak self]phone in
            self?.phoneTextField.text = phone
        }
        let nav = BaseNavigationController(rootViewController: sign)
        self.present(nav, animated: true) {
            
        }
    }
    
    //注册
    @IBAction func signUpButtonClick(_ sender: Any) {
        let sign = SignUpViewController()
        sign.signupSuccessBlock = { [weak self]phone in
            self?.phoneTextField.text = phone
        }
        let nav = BaseNavigationController(rootViewController: sign)
        self.present(nav, animated: true) {
            
        }
    }
    
    //登录
    @IBAction func loginButtonClick(_ sender: Any) {
        login()
    }
}

//MARK: UI
extension SignInViewController{
    private func setupUI(){

        self.view.backgroundColor = UIColor.white
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: loginButton.frame.size)
        loginButton.setBackgroundImage(image, for: UIControlState.normal)
        loginButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
    }
}

//MARK: 网络请求
extension SignInViewController{
    //登录
    private func login(){
        var param = [String:String]()
        param["type"] = "3"
        
        if let mobile = phoneTextField.text{
            if mobile.isEmpty(){
                showMessageHUD("请填写手机号")
                return
            }else{
                if !Validate.phoneNum(mobile).isRight{
                    showMessageHUD("请填写正确的手机号")
                    return
                }else{
                    param["mobile"] = mobile
                }
            }
        }
        
        if let pwd = pwdTextField.text{
            if pwd.isEmpty(){
                showMessageHUD("请输入密码")
                return
            }else{
                param["password"] = pwd
            }
        }
        
        showLoadingHUD("")
        NetWorkRequestTool.request(url: accountLogin, methodType: .post, parameters: param) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let info = UserInfo.deserialize(from: data) else{
                    return
                }
                UserInfoTool.saveUserInfo(user: info)
                self?.getInfo(io: info)
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
    //获取个人信息
    private func getInfo(io : UserInfo){
        showLoadingHUD("初始化个人信息")
        NetWorkRequestTool.request(url: accountProfile, methodType: .get, parameters: [:]) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let info = UserInfo.deserialize(from: data) else{
                    return
                }
                info.token = io.token
                info.address = io.address
                UserInfoTool.saveUserInfo(user: info)
                self?.dismiss(animated: true) {
                    
                }
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
        
    }
}
