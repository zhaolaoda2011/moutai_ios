//
//  AddAddressViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit
import HandyJSON

//struct AddAddressParam :  HandyJSON{
//    var province_id : Int = -1
//    var city_id : Int = -1
//    var district_id : Int = -1
//    var address1 : String = ""
//    var address2 : String = ""
//    var is_default : Bool = false
//    var name : String = ""
//    var mobile : String = ""
//}

class AddAddressViewController: BaseViewController {
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var defaultButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var detailAddressLabel: UITextField!
    
    ///是否是编辑状态
    var isModify : Bool = false
    
    //用于编辑
    var model : AddressInfo = AddressInfo()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    ///确认
    @IBAction func submitButtonClick(_ sender: Any) {
        if let name = nameTextField.text{
            if !name.isEmpty(){
                model.name = name
            }else{
                showMessageHUD("请输入收货人")
                return
            }
        }
        
        if let phone = phoneTextField.text{
            if !phone.isEmpty(){
                if Validate.phoneNum(phone).isRight{
                    model.mobile = phone
                }else{
                    showMessageHUD("请输入正确的电话")
                    return
                }
            }else{
                showMessageHUD("请输入电话")
                return
            }
        }
        
        if model.city_id < 0 || model.province_id < 0 || model.district_id < 0{
            showMessageHUD("请选择省市区")
            return
        }else{
//            model.address1 = addressLabel.text ?? ""
        }
        
        if let address = detailAddressLabel.text{
            if !address.isEmpty(){
                model.address = address
            }else{
                showMessageHUD("请输入详细地址")
                return
            }
        }
        
        model.is_default = defaultButton.isSelected
        
        var param = model.toJSON()
        if isModify{
            param!["id"] = "\(model.id)"
        }else{
            param?.removeValue(forKey: "id")
        }
        
        showMessageHUD(isModify ? "正在编辑地址" : "正在新增地址")
        NetWorkRequestTool.request(url:addresssave, methodType: .post, parameters: param) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                showMessageHUD((self?.isModify)! ? "编辑地址成功" : "新增地址成功")
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    ///默认地址
    @IBAction func defaultButtonClick(_ sender: Any) {
        let button = sender as! UIButton
        button.isSelected = !button.isSelected
        model.is_default = button.isSelected
    }
    
    ///点击了选择地址
    @IBAction func addressLabelClick(_ sender: Any) {
        showLoadingHUD("")
        DispatchQueue(label: "address").async {
            DistrictManager.shareInstance.getDistrictData { (result) in
                DispatchQueue.main.async {
                    hideAllHUD()
                    let city = CityPickerView()
                    city.dataArray = result
                    city.submitBlock = {[weak self](province,city,distinct)in
                        self?.addressLabel.text = "\(String(describing: province.name!)) \(String(describing: city.name!)) \(String(describing: distinct.name!))"
                        self?.model.province_id = province.code
                        self?.model.city_id = city.code
                        self?.model.district_id = distinct.code
                    }
                    city.show()
                }
            }
        }
        
    }
}

//MARK: ui
extension AddAddressViewController{
    private func setupUI(){
        self.title = isModify  ? "编辑地址" : "添加新地址"
        self.view.backgroundColor = UIColor.white
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: submitButton.frame.size)
        submitButton.setBackgroundImage(image, for: UIControlState.normal)
        submitButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
        
        if isModify{
            nameTextField.text = model.name
            phoneTextField.text = model.mobile
//            addressLabel.text = model.address1
            DistrictManager.shareInstance.getAddressByCode(model.province_id, model.city_id, model.district_id) { [weak self](str) in
                self?.addressLabel.text = str
            }
            
            detailAddressLabel.text = model.address
            defaultButton.isSelected = model.is_default
        }
    }
}
