//
//  SignUpViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var buttonContainView: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var recommenMobileTextField: UITextField!
    
    var isGetCode : Bool = false
    var signupSuccessBlock : ((_ phone : String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //注册
    @IBAction func submitButtonClick(_ sender: Any) {
        var param = [String:String]()

        if let mobile = phoneTextField.text{
            if mobile.isEmpty(){
                showMessageHUD("请填写手机号")
                return
            }else{
                if !Validate.phoneNum(mobile).isRight{
                    showMessageHUD("请填写正确的手机号")
                    return
                }else{
                    param["mobile"] = mobile
                }
            }
        }
        
        if !isGetCode{
            showMessageHUD("请获取验证码")
            return
        }
        
        if let code = codeTextField.text{
            if code.isEmpty(){
                showMessageHUD("请填写验证码")
                return
            }else{
                param["verify_code"] = code
            }
        }
        
        if let pwd = pwdTextField.text{
            if pwd.isEmpty(){
                showMessageHUD("请输入密码")
                return
            }else{
                param["password"] = pwd
            }
        }
        
        if let pmobile = recommenMobileTextField.text{
            if pmobile.isEmpty(){
                showMessageHUD("请输入推荐人手机号")
                return
            }else{
                if !Validate.phoneNum(pmobile).isRight{
                    showMessageHUD("请填写正确的推荐人手机号")
                    return
                }else{
                    param["p_mobile"] = pmobile
                }
            }
        }
        
//        if let pmobile = recommenMobileTextField.text{
//            if !pmobile.isEmpty(){
//                if !Validate.phoneNum(pmobile).isRight{
//                    showMessageHUD("请填写正确的推荐人手机号")
//                    return
//                }else{
//                    param["p_mobile"] = pmobile
//                }
//            }
//        }
        
        NetWorkRequestTool.request(url: accountRegister, methodType: .post, parameters: param) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                showMessageHUD("注册成功")
                if let block = self?.signupSuccessBlock {
                    block(self?.phoneTextField.text ?? "")
                }
                self?.dismiss(animated: true, completion: {
                })
                
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
    //验证码
    @IBAction func getCodeButtonClick(_ sender: Any) {
        let button = sender as! UIButton
        
        
        var param = [String:String]()
        if let mobile = phoneTextField.text{
            if mobile.isEmpty(){
                showMessageHUD("请填写手机号")
                return
            }else{
                if !Validate.phoneNum(mobile).isRight{
                    showMessageHUD("请填写正确的手机号")
                    return
                }else{
                    param["mobile"] = mobile
                }
            }
        }
        
        param["type"] = "1"
        
        NetWorkRequestTool.request(url: accountVerifyCode, methodType: .post, parameters: param) { (result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                showMessageHUD("获取验证码成功,请注意查收")
                button.startSMSWithDuration(duration: 30)
                self.isGetCode = true
            }else{
                NetWorkRequestTool.showRequestMessage(result)

            }
        }
    }
}

//MARK: UI
extension SignUpViewController{
    private func setupUI(){
        self.title = "注册"
        
        self.view.backgroundColor = UIColor.white
        
        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: submitButton.frame.size)
        submitButton.setBackgroundImage(image, for: UIControlState.normal)
        submitButton.clipsToBounds = true
        
        buttonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        buttonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        buttonContainView.layer.shadowOpacity = 0.5
        buttonContainView.layer.cornerRadius = 4
        
        //添加后退按钮
        let button = UIButton(imageName: "ic_back", highImageName: "ic_back")
        button.sizeToFit()
        button.imageEdgeInsets = UIEdgeInsetsMake(0, -15, 0, 0)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        button.addTarget(self, action: #selector(customerbackClick), for: UIControlEvents.touchUpInside)
    }
    
    @objc func customerbackClick(){
        self.dismiss(animated: true) {
            
        }
    }
}
