//
//  TeamCell.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class TeamCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
