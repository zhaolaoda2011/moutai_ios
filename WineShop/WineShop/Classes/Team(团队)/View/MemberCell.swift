//
//  MemberCell.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class MemberCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileLabel: UILabel!
    
    var model : MemberInfo?{
        didSet{
            guard let mem = model else{
                return
            }
            NetWorkRequestTool.downloadImageToImageView(iconImageView, mem.avatar_url)
            nameLabel.text = mem.username
            mobileLabel.text = mem.mobile
        }
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        iconImageView.layer.cornerRadius = iconImageView.bounds.width/2.0
        iconImageView.layer.masksToBounds = true
    }
    
}
