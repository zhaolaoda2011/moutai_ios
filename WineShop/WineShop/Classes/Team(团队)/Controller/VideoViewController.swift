//
//  VideoViewController.swift
//  WineShop
//
//  Created by zc on 2018/6/7.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit
import WebKit

class VideoViewController: BaseViewController {
    var html : String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
}

//MARK: ui
extension VideoViewController{
    private func setupUI(){
        self.title = "学习视频"
        self.view.backgroundColor = UIColor.black
        
        let view = WKWebView(frame: self.view.bounds)
        view.loadHTMLString(html, baseURL: nil)
//        let pattern = "https.*?\\\""
//        do {
//            let pat = try NSRegularExpression(pattern: pattern, options: [])
//            let results = pat.matches(in: html, options: [], range: NSRange(location: 0, length: html.count))
//            if results.count >= 0 {
//                guard let r = results.first else{
//                    return
//                }
//                let chs = (html as NSString).substring(with: r.range)
//                let urlstr = (chs as NSString).replacingOccurrences(of: "\\", with: "").replacingOccurrences(of: "\"", with: "")
//                guard let url = URL(string: urlstr)else{
//                    return
//                }
//                let request = URLRequest(url: url)
//                view.load(request)
//            }
//        } catch  {
//            print(error.localizedDescription)
//        }
        self.view.addSubview(view)
    }
}
