//
//  TeamViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class TeamViewController: BaseViewController {
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI() 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationStyle(show: true)
        if UserInfoTool.checkUserLogin(){
            getfx()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setupNavigationStyle(show: false)
    }
    
    //一级成员
    @IBAction func firstLevelButtonClick(_ sender: Any) {
        if !UserInfoTool.checkUserLogin(){
            let login = SignInViewController()
            self.present(login, animated: true) {
                
            }
            return
        }
        let mem = MemberViewController()
        mem.title = "一级成员"
        mem.level = "1"
        self.navigationController?.pushViewController(mem, animated: true)
    }
    
    //二级成员
    @IBAction func secondLevelButtonClick(_ sender: Any) {
        if !UserInfoTool.checkUserLogin(){
            let login = SignInViewController()
            self.present(login, animated: true) {
                
            }
            return
        }
        let mem = MemberViewController()
        mem.title = "二级成员"
        mem.level = "2"
        self.navigationController?.pushViewController(mem, animated: true)
    }
}

//MARK: ui
extension TeamViewController{
    private func setupUI(){
        mainTableView.register(UINib(nibName: "TeamCell", bundle: nil), forCellReuseIdentifier: "TeamCell")
    }
    
    ///设置导航栏颜色 true为蓝色  false为白色
    func setupNavigationStyle(show:Bool){
        //导航栏背景颜色
        let bgColor = show ? UIColor.withHex(hexInt: 0xED6469) : UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.createImage(color: bgColor), for: .default)
        //导航栏线的颜色
        let lineColor = show ? UIColor.withHex(hexInt: 0xED6469) : UIColor.withHex(hexInt: 0xE8E8E8)
        self.navigationController?.navigationBar.shadowImage = UIImage.createImage(color: lineColor)
        //导航栏字体颜色
        let titleFont = kFontWithSize(size: 18)
        let titleColor = show ? UIColor.white : kFontColor
        let titleAttributeDict = [NSAttributedStringKey.foregroundColor : titleColor,NSAttributedStringKey.font : titleFont]
        self.navigationController?.navigationBar.titleTextAttributes = titleAttributeDict
        //状态栏颜色
        UIApplication.shared.statusBarStyle = show ? UIStatusBarStyle.lightContent : UIStatusBarStyle.default
    }
}

//MARK: tableview delegate datasource
extension TeamViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell") as! TeamCell
//        cell.iconImageView.image = UIImage(named: indexPath.row == 0 ? "ic_erweima" : "ic_shipin")
//        cell.nameLabel.text = indexPath.row == 0 ? "二维码推广" : "学习视频"
//        return cell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamCell") as! TeamCell
        cell.iconImageView.image = UIImage(named: "ic_shipin")
        cell.nameLabel.text =  "学习视频"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        if indexPath.row == 0{
//            if !UserInfoTool.checkUserLogin(){
//                let login = SignInViewController()
//                self.present(login, animated: true) {
//
//                }
//                return
//            }
//            getCode()
//        }else{
            getVideo()
//        }
    }
}

//MARK: 网络请求
extension TeamViewController{
    private func getfx(){
        NetWorkRequestTool.request(url: fxinfo, methodType: .get, parameters: [:]) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let info = FxInfo.deserialize(from: data) else{
                    return
                }
                self?.firstLabel.text = "\(info.first)"
                self?.secondLabel.text = "\(info.second)"

            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
    private func getVideo(){
        showLoadingHUD("")
        NetWorkRequestTool.request(url: systemvideo, methodType: .get, parameters: [:]) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? String else{
                    return
                }
                if !data.isEmpty{
                    let video = VideoViewController()
                    let url = (data as NSString).replacingOccurrences(of: "\\", with: "")
                    let iframe = "<iframe  width=\(kScreenWidth()) src='\(url)' frameborder=0 'allowfullscreen' style='position: absolute; top: 50%;transform: translateY(-50%);'></iframe>"
                    let html = "<!DOCTYPE HTML><html><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no\"><style>body{margin:0 auto; background-color: black; }</style><body>\(iframe)</body></html>"
                    video.html = html
                    self?.navigationController?.pushViewController(video, animated: true)
                }
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
    private func getCode(){
        showLoadingHUD("")
        NetWorkRequestTool.request(url: systemqrCode, methodType: .get, parameters: [:]) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? String else{
                    return
                }
                if !data.isEmpty{
                    let url = (data as NSString).replacingOccurrences(of: "\\", with: "")
                    let code = QRCodeViewController()
                    code.url = url
                    self?.navigationController?.pushViewController(code, animated: true)
                }
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
}


