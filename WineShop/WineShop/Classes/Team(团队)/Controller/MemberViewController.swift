//
//  MemberViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class MemberViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    var level : String = ""
    var memberArray = [MemberInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupMJRefresh(tableView, headerHeight: 0, showRefreshHeader: true, showRefreshFooter: true)
        getfxList()
    }

    ///加载数据
    override func loadData(_ isRefresh: Bool) {
        super.loadData(isRefresh)
        if isRefresh{
            //            salesArray = [SaleCompany]()
            //            tableView.reloadData()
        }
        getfxList()
    }
}

//MARK: ui
extension MemberViewController{
    private func setupUI(){
        tableView.register(UINib(nibName: "MemberCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
    }
}

//MARK: tableview delegate datasource
extension MemberViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return memberArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell") as! MemberCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.model = memberArray[indexPath.row]
        cell.backgroundColor = UIColor.white
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

//MARK: 网络请求
extension MemberViewController{
    private func getfxList(){
        let param = ["level" : level,"pageSize":"\(kPageSize)",
                     "page":"\(page)"]
        NetWorkRequestTool.request(url: fxList, methodType: .get, parameters: param) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let pageModel = NetWorkRequestTool.getPageModel(result)else{
                    return
                }
                self?.page = pageModel.current_page
                self?.last_page = pageModel.last_page
                
                guard let list = [MemberInfo].deserialize(from: pageModel.data) else{
                    return
                }
                
                if self?.page == 1 {
                    self?.memberArray.removeAll()
                    self?.tableView.mj_header.isHidden = !(list.count>0)
                    self?.tableView.mj_footer.isHidden = !(list.count>0)
                }
                
                list.forEach({ (sale) in
                    self?.memberArray.append(sale!)
                })
                
                self?.tableView.reloadData()
                
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
            self?.tableView.mj_header.endRefreshing()
            self?.tableView.mj_footer.endRefreshing()
        }
    }
}
