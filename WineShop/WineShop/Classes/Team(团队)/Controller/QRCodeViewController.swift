//
//  QRCodeViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class QRCodeViewController: BaseViewController {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var shareButtonContainView: UIView!
    var url : String! = ""
    @IBOutlet weak var codeImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

}

//MARK: UI
extension QRCodeViewController{
    private func setupUI(){
        self.title = "二维码推广"
        
        iconImageView.layer.cornerRadius = iconImageView.frame.width/2.0
        iconImageView.layer.masksToBounds = true
        
        containView.layer.shadowColor = UIColor.withHex(hexInt: 0xF3F3F3).cgColor
        containView.layer.shadowOffset = CGSize(width: 0, height: 0)
        containView.layer.shadowOpacity = 1
        containView.layer.cornerRadius = 4
        
        shareButton.clipsToBounds = true
        
        shareButtonContainView.layer.shadowColor = UIColor.withHex(hexInt: 0xFF6565, alpha: 0.28).cgColor
        shareButtonContainView.layer.shadowOffset = CGSize(width: 0, height: 9)
        shareButtonContainView.layer.shadowOpacity = 0.5
        shareButtonContainView.layer.cornerRadius = 4

        let image = UIImage.init(gradientColors: [UIColor.withHex(hexInt: 0xED6369),UIColor.withHex(hexInt: 0xFF9E82)], size: shareButton.frame.size)
        shareButton.setBackgroundImage(image, for: UIControlState.normal)
        
        NetWorkRequestTool.downloadImageToImageView(codeImageView, url)
        
        let info = UserInfoTool.loadUserInfo()
        NetWorkRequestTool.downloadImageToImageView(iconImageView, info.avatar_url ?? "", "ic_man_moren")
        nameLabel.text = info.username ?? ""
    }
}
