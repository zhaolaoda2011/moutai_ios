//
//  TipView.swift
//  juzhuanwang
//
//  Created by zc on 2018/4/24.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

enum TipViewType : Int {
    ///空数据
    case TipViewTypeEmptyData = 0
    ///加载失败
    case TipViewTypeLoadFailed
    ///网络错误
    case TipViewTypeNetWorkError
}

class TipView: UIView {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var tipLabel: UILabel!
    
    var tipType : TipViewType = TipViewType.TipViewTypeEmptyData{
        willSet{
            switch newValue {
            case .TipViewTypeEmptyData:
                iconImageView.image = UIImage(named: "emptyData")
                tipLabel.text = "空空如也"
                break
            case .TipViewTypeLoadFailed:
                iconImageView.image = UIImage(named: "loadFailed")
                tipLabel.text = "加载出了点小问题"
                break
            case .TipViewTypeNetWorkError:
                iconImageView.image = UIImage(named: "netWorkError")
                tipLabel.text = "网络开了小差"
                break
            }
            self.layoutIfNeeded()
        }
    }
    
    static func initView()->TipView{
         return UINib(nibName: "TipView", bundle: Bundle.main).instantiate(withOwner: nil, options: nil).first as! TipView
    }
}
