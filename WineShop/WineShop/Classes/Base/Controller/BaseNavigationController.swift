//
//  BaseNavigationController.swift
//  Album
//
//  Created by zc on 2017/11/2.
//  Copyright © 2017年 zj. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //主体颜色
        UINavigationBar.appearance().tintColor = kFontColor
        //设置标题字体和颜色 还有阴影颜色
        let titleColor = kFontColor
        let titleFont = kFontWithSize(size: 18)//kFontBlodWithSize(size: 20)
//        let shadow = NSShadow()
//        shadow.shadowColor = UIColor.withRGBA(224, 224, 224, 1)
        let titleAttributeDict = [NSAttributedStringKey.foregroundColor : titleColor,NSAttributedStringKey.font : titleFont]
        self.navigationBar.titleTextAttributes = titleAttributeDict
        //self.navigationBar.shadowImage = UIImage.createImage(color: UIColor.withHex(hexInt: 0xE8E8E8))
        //导航栏线的颜色
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage.createImage(color: UIColor.withHex(hexInt: 0xE8E8E8))
        //后退按钮
//        UINavigationBar.appearance().bac
    }
    
    
    
    //每一次push都会执行这个方法，push之前设置viewController的hidesBottomBarWhenPushed
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        viewController.hidesBottomBarWhenPushed = true
        super.pushViewController(viewController, animated: true)
        viewController.hidesBottomBarWhenPushed = false
        
    }
    
    override func popViewController(animated: Bool) -> UIViewController? {
        
        //以下函数是对返回上一级界面之前的设置操作
        //每一次对viewController进行push的时候，会把viewController放入一个栈中
        //每一次对viewController进行pop的时候，会把viewController从栈中移除
        if self.childViewControllers.count == 2
        {
            //如果viewController栈中存在的ViewController的个数为两个，再返回上一级界面就是根界面了
            //那么要对tabbar进行显示
            let controller:UIViewController = self.childViewControllers[0]
            controller.hidesBottomBarWhenPushed = false
            
        }
        else
        {
            //如果viewController栈中存在的ViewController的个数超过两个，对要返回到的上一级的界面设置hidesBottomBarWhenPushed = true
            //把tabbar进行隐藏
            let count = self.childViewControllers.count-2
            let controller = self.childViewControllers[count]
            controller.hidesBottomBarWhenPushed = true
        }
        
        
        return super.popViewController(animated: true)
    }

}
