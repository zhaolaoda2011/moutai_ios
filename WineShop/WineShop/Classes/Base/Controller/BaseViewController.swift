//
//  BaseViewController.swift
//  Album
//
//  Created by zc on 2017/11/2.
//  Copyright © 2017年 zj. All rights reserved.
//

import UIKit
import MJRefresh
import Alamofire

class BaseViewController: UIViewController,UIGestureRecognizerDelegate {
    var contentView : UIView?

    ///当前页数
    var page : Int = 1
    ///所有页数
    var last_page : Int = 1

    
    ///提示view
    lazy var baseTipView : TipView = {
        let tip = TipView.initView()
        return tip
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = kControllerBGColor
        if contentView == nil {
            contentView = self.view
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(networkErrorAction(_:)), name: NSNotification.Name(rawValue: NetWorkRequestTool.kNetWorkErrorNotificationKey), object: nil)
        
        initNavgationBar()
        
        //侧滑后退
        guard let nav = self.navigationController else{
            return
        }
        
        if nav.responds(to: #selector(getter: UINavigationController.interactivePopGestureRecognizer)){
            if nav.viewControllers.count > 1{
                nav.interactivePopGestureRecognizer?.delegate = self
            }
        }
        
//        if #available(iOS 11.0, *) {
//            tableView.contentInsetAdjustmentBehavior = .never
//        } else {
//            self.automaticallyAdjustsScrollViewInsets = false
//        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle{
//        get {
//            return.lightContent
//
//        }
//
//    }
    
    ///加载数据
    func loadData(_ isRefresh : Bool){
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NetWorkRequestTool.kNetWorkErrorNotificationKey), object: nil)
    }
}

// MARK: 导航栏相关
extension BaseViewController{
    ///初始化导航栏
    func initNavgationBar(){
        guard let nav = self.navigationController else{
            return
        }
        if((nav.viewControllers.count) > 1){
            //添加后退按钮
            let button = UIButton(imageName: "ic_back", highImageName: "ic_back")
            button.sizeToFit()
            var frame = button.frame
            frame.size.width += 30
            frame.size.height += 20
            button.frame = frame
            button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            button.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
            button.addTarget(self, action: #selector(backClick), for: UIControlEvents.touchUpInside)
        }
    }
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: 刷新 加载
extension BaseViewController{
    ///设置 刷新与 加载
    func setupMJRefresh(_ tableView : UITableView,headerHeight : CGFloat,showRefreshHeader : Bool,showRefreshFooter : Bool){

        tableView.estimatedRowHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        if #available(iOS 11.0, *) {
            tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
        if showRefreshHeader{
            let header = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(refreshAction))
            header?.isAutomaticallyChangeAlpha = true
            header?.lastUpdatedTimeLabel.isHidden = true
            header?.isHidden = true
            tableView.mj_header = header
            if headerHeight > 0.0{
                tableView.mj_header.mj_h = headerHeight
            }
        }
        
        if showRefreshFooter{
            let footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(loadAction(sender:)))
            footer?.setTitle("上拉加载更多", for: MJRefreshState.idle)
            footer?.isHidden = true
            tableView.mj_footer = footer
            tableView.reloadData()
        }
    }
    
    ///设置 刷新与 加载
    func setupMJRefresh(_ collectionView : UICollectionView,headerHeight : CGFloat,showRefreshHeader : Bool,showRefreshFooter : Bool){

        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentBehavior.never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
        if showRefreshHeader{
            let header = MJRefreshNormalHeader(refreshingTarget: self, refreshingAction: #selector(refreshAction))
            header?.isAutomaticallyChangeAlpha = true
            header?.lastUpdatedTimeLabel.isHidden = true
            header?.isHidden = true
            collectionView.mj_header = header
            if headerHeight > 0.0{
                collectionView.mj_header.mj_h = headerHeight
            }
        }
        
        if showRefreshFooter{
            let footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(loadAction(sender:)))
            footer?.setTitle("上拉加载更多", for: MJRefreshState.idle)
            footer?.isHidden = true
            collectionView.mj_footer = footer
            collectionView.reloadData()
        }
    }
    
    
    
    @objc func refreshAction(){
        page = 1
        last_page = 1
        loadData(true)
    }
    
    //sender : MJRefreshAutoNormalFooter
    @objc func loadAction(sender : MJRefreshAutoNormalFooter){
        if page+1 <= last_page{
            page += 1
            loadData(false)
        }else{
            sender.endRefreshingWithNoMoreData()
        }
    }
}

//MARK: - 展示提示view
extension BaseViewController {
    ///网络异常回调
    @objc func networkErrorAction(_ notification : Notification){
        guard let status = notification.object as? NetworkReachabilityManager.NetworkReachabilityStatus else {
            return
        }
        switch status {
        case .notReachable:
            showTipView(true, TipViewType.TipViewTypeNetWorkError, self.view.bounds)
            break
        case .unknown:
            showTipView(true, TipViewType.TipViewTypeNetWorkError, self.view.bounds)
            break
        case .reachable(NetworkReachabilityManager.ConnectionType.ethernetOrWiFi):
            if self.baseTipView.tipType == TipViewType.TipViewTypeNetWorkError{
                showTipView(false, TipViewType.TipViewTypeNetWorkError, self.view.bounds)
            }
            break
        case .reachable(NetworkReachabilityManager.ConnectionType.wwan):
            if self.baseTipView.tipType == TipViewType.TipViewTypeNetWorkError{
                showTipView(false, TipViewType.TipViewTypeNetWorkError, self.view.bounds)
            }
            break
        }
    }
    ///显示 或 隐藏 提示view
    func showTipView(_ show : Bool,_ type : TipViewType,_ frame : CGRect){
        self.baseTipView.tipType = type
        self.baseTipView.frame = frame
        if show{
//            if !self.baseTipView.isHidden{
                self.view.addSubview(self.baseTipView)
//            }
        }else{
            self.baseTipView.removeFromSuperview()
        }
        self.baseTipView.isHidden = !show
        self.view.bringSubview(toFront: self.baseTipView)
    }
}

//MARK: - Alter
extension BaseViewController {
    func showAlterWithTitle(_ title: String,
                            message: String,
                            cancelTitle: String,
                            defaultTitle: String,
                            cancelBlock : @escaping () -> (),
                            defaultBlock: @escaping () -> ()
        ) {
        let alertController = UIAlertController(title: title,
                                                message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: defaultTitle, style: .default, handler: {
            action in
            defaultBlock()
        })
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: {
            action in
            cancelBlock()
        })
        alertController.addAction(cancelAction)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlterWithDefaultAction(_ title: String,
                            message: String,
                            defaultTitle: String,
                            defaultBlock: @escaping () -> ()
        ) {
        let alertController = UIAlertController(title: title,
                                                message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: defaultTitle, style: .default, handler: {
            action in
            defaultBlock()
        })
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
