//
//  HomeTabbarController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class HomeTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTabbarUI()
        setupController()
    }
}

extension HomeTabbarController{
    //设置外观
    private func setupTabbarUI(){
        tabBar.shadowImage = UIImage.createImage(color: UIColor.clear)
        tabBar.isTranslucent = false
        tabBar.backgroundImage = UIImage.createImage(color: UIColor.withHex(hexInt: 0xFAFAFA))
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.withHex(hexInt: 0x999999)], for: UIControlState.normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.withHex(hexInt: 0xE55157)], for: UIControlState.selected)
    }
    
    //添加子控制器
    private func addChildController(controller : UIViewController,title : String,imageName : String)->BaseNavigationController{
        controller.title = title
        controller.tabBarItem.image = UIImage(named: "ic_tab_\(imageName)_nor")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        controller.tabBarItem.selectedImage = UIImage(named: "ic_tab_\(imageName)_sel")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        //controller.tabBarItem.imageInsets = UIEdgeInsets(top: 5, left: 0, bottom: -5, right: 0)
        let nav = BaseNavigationController(rootViewController: controller)
        //nav.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -4)
        nav.navigationBar.isTranslucent = false
        
//        self.addChildViewController(nav)
        return nav
    }
    
    func setupController(){
        if SystemConfigManager.shareInstance.systemConfigModel.is_prod{
            self.viewControllers = [addChildController(controller: ProdutctViewController(), title: "产品",imageName: "intuction"),addChildController(controller: TeamViewController(), title: "团队",imageName: "team"),addChildController(controller: MyViewController(), title: "我的",imageName: "mine")]
        }else{
            self.viewControllers = [addChildController(controller: ProdutctViewController(), title: "产品",imageName: "intuction"),addChildController(controller: MyViewController(), title: "我的",imageName: "mine")]
        }
    }
}
