//
//  ProductCell.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCell: UICollectionViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var model : Product?{
        willSet{
            guard let m = newValue else{
                return
            }
            nameLabel.text = m.name
            let image = SDWebImageManager.shared().imageCache?.imageFromCache(forKey: m.url)
            iconImageView.image = image
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
