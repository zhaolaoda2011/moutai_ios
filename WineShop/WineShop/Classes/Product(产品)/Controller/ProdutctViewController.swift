//
//  ProdutctViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit
import CHTCollectionViewWaterfallLayout
import SDWebImage

class ProdutctViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    lazy var animator = ProductDetailAnimator()
    
    var productArray = [Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        ///设置刷新
        setupMJRefresh(collectionView, headerHeight: 0, showRefreshHeader: true, showRefreshFooter: true)
        collectionView.mj_header.isHidden = false
//        self?.collectionView.mj_footer.isHidden = !(list.count>0)
        /// 获取商品列表
        loadData(true)
    }
    
    ///加载数据
    override func loadData(_ isRefresh: Bool) {
        super.loadData(isRefresh)
        if isRefresh{
            //            salesArray = [SaleCompany]()
            //            tableView.reloadData()
        }
        getProducList()
    }
}

//MARK: 设置UI
extension ProdutctViewController{
    private func setupUI(){
        self.view.backgroundColor = UIColor.white
        
        //collection view 布局
        let layout = CHTCollectionViewWaterfallLayout()
        layout.columnCount = 2
        layout.minimumInteritemSpacing = 10
        layout.minimumColumnSpacing = 10
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "ProductCell")
        
        self.navigationController?.delegate = self
    }
}

//MARK: collectionview delegate datasource
extension ProdutctViewController:UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        cell.model = productArray[indexPath.row]
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let model = productArray[indexPath.row]
        
        let w = (kScreenWidth()-10*3)/2.0
        var h : CGFloat = 0
        
        if let image = SDWebImageManager.shared().imageCache?.imageFromCache(forKey: model.url){
            let imageHeight = w / image.size.width * image.size.height
            h += imageHeight
        }else{
            h += w
        }
        
        let title = model.name
        let titleHeight = title.height(withConstrainedWidth: w, font: kFontWithSize(size: 15))
        h += titleHeight
        
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        animator.indexPath = indexPath
        animator.pushDelegate = self
        let detail = ProductDetailViewController()
        detail.hidesBottomBarWhenPushed = true
        detail.model = productArray[indexPath.row]
        detail.indexPath = indexPath
        animator.pushToDelegate = detail
        animator.popDelegate = detail
        self.navigationController?.pushViewController(detail, animated: true)
    }
}


//MARK: 导航栏代理
extension ProdutctViewController:UINavigationControllerDelegate,ProductDetailAnimatorPushDelegate{
    func productDetailAnimatorPushDelegatePushFromRect(indexPath: IndexPath) -> CGRect {
        let cell = collectionView.cellForItem(at: indexPath) as! ProductCell
        let rect = cell.convert(cell.iconImageView.frame, to: UIApplication.shared.keyWindow)
        return rect
    }
    
    func productDetailAnimatorPushDelegatePushToRect(indexPath: IndexPath) -> CGRect {
        let model = productArray[indexPath.row]
        
        let maxW = kScreenWidth()
        let maxHeight = maxW * 325.0 / 375.0
        
        
        var x : CGFloat = 0
        var y : CGFloat = 0
        var w : CGFloat = 0
        var h : CGFloat = 0
        
        if let image = SDWebImageManager.shared().imageCache?.imageFromCache(forKey: model.url){
            if image.size.width > image.size.height{
                x = 0
                w = maxW
                h = w / image.size.width * image.size.height
                y = (maxHeight - h)/2.0+64
            }else{
                y = 0+64
                h = maxHeight
                w = h / image.size.height * image.size.width
                x = (maxW-w)/2.0
            }
        }
        
        return CGRect(x: x, y: y, width: w, height: h)
    }
    
    func productDetailAnimatorPushDelegatePushFromView(indexPath: IndexPath) -> UIImageView {
        let model = productArray[indexPath.row]
        let image = SDWebImageManager.shared().imageCache?.imageFromCache(forKey: model.url)
        let imageView = UIImageView()
        imageView.image = image
        return imageView
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == UINavigationControllerOperation.push{
            if fromVC.isKind(of: ProdutctViewController.self){
                animator.operation = operation
                return animator
            }else{
                return nil
            }
        }else{
            if !fromVC.isKind(of: ProductDetailViewController.self){
                return nil
            }else{
                animator.operation = operation
                return animator
            }
        }
        
    }
}

//MARK: 网络请求
extension ProdutctViewController{
    func getProducList(){
        let params = [
            "pageSize":kPageSize,
            "page":page
            ]
        NetWorkRequestTool.request(url: productlist, methodType: .get, parameters: params) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let pageModel = NetWorkRequestTool.getPageModel(result)else{
                    return
                }
                self?.page = pageModel.current_page
                self?.last_page = pageModel.last_page
                
                guard let list = [Product].deserialize(from: pageModel.data) else{
                    return
                }
                
                if self?.page == 1 {
                    self?.productArray.removeAll()
                    self?.collectionView.mj_header.isHidden = !(list.count>0)
                    self?.collectionView.mj_footer.isHidden = !(list.count>0)
                }
                
                list.forEach({ (sale) in
                    self?.productArray.append(sale!)
                })
                
                self?.loadImage(array: list as? [Product])
                
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
            self?.collectionView.mj_header.endRefreshing()
            self?.collectionView.mj_footer.endRefreshing()
        }
    }
    
    //获取图片
    func loadImage(array : [Product]?){
        guard let list = array else{
            return
        }
        let group = DispatchGroup()
        for product in list{
            group.enter()
            SDWebImageManager.shared().loadImage(with: URL(string: product.url), options: [], progress: { (current, total, url) in
                
            }) { (image, data, error, type, finish, url) in
                group.leave()
            }
        }
        
        group.notify(queue: DispatchQueue.main) {
            self.collectionView.reloadData()
        }
    }
}
