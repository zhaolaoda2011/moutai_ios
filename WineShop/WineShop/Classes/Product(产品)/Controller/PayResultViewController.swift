//
//  PayResultViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class PayResultViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
    }
    
    //返回首页
    @IBAction func backToHomeButtonClick(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
}
