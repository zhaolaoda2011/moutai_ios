//
//  ProductDetailAnimator.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

protocol ProductDetailAnimatorPushDelegate {
    func productDetailAnimatorPushDelegatePushFromRect(indexPath : IndexPath)->CGRect
    func productDetailAnimatorPushDelegatePushToRect(indexPath : IndexPath)->CGRect
    func productDetailAnimatorPushDelegatePushFromView(indexPath : IndexPath)->UIImageView
}

protocol ProductDetailAnimatorPushToDelegate {
    func productDetailAnimatorPushToDelegateToView()->UIView
}

protocol ProductDetailAnimatorPopDelegate {
    func productDetailAnimatorPopDelegateFromIndexPath()->IndexPath
    func productDetailAnimatorPopDelegateFromView()->UIImageView
}

class ProductDetailAnimator: NSObject,UIViewControllerAnimatedTransitioning {
    ///动画时间
    let animationDurantion = 0.5
    ///判断是push还是pop
    var operation : UINavigationControllerOperation = UINavigationControllerOperation.push
    ///保存上下文
    weak var storeContext : UIViewControllerContextTransitioning?
    ///从那个indexpath开始做动画
    var indexPath : IndexPath?
    ///push的delegate
    var pushDelegate : ProductDetailAnimatorPushDelegate?
    ///push到对应controller的delegate
    var pushToDelegate : ProductDetailAnimatorPushToDelegate?
    ///pop的delegate
    var popDelegate : ProductDetailAnimatorPopDelegate?
    
    ///动画时间
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationDurantion
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        storeContext = transitionContext
        
        switch operation {
        case .push:
            let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
            
            let startRect = pushDelegate?.productDetailAnimatorPushDelegatePushFromRect(indexPath: indexPath!)
            let endRect = pushDelegate?.productDetailAnimatorPushDelegatePushToRect(indexPath: indexPath!)
            let imageView = pushDelegate?.productDetailAnimatorPushDelegatePushFromView(indexPath: indexPath!)
            imageView?.frame = startRect!
            let toView = pushToDelegate?.productDetailAnimatorPushToDelegateToView()
            
            let toVCView = toVC.view!
            toVCView.frame = transitionContext.finalFrame(for: toVC)
            
            transitionContext.containerView.addSubview(toVCView)
            transitionContext.containerView.addSubview(imageView!)
            
            toVCView.alpha = 0
            toView?.isHidden = true
            UIView.animate(withDuration: animationDurantion, animations: {
                imageView?.frame = endRect!
                toVCView.alpha = 1
            }) { (isFinished) in
                imageView?.removeFromSuperview()
                toView?.isHidden = false
                transitionContext.completeTransition(true)
            }
            break
        case .pop:
            let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
            let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
            transitionContext.containerView.insertSubview(toView, belowSubview: fromView)
            
            let imageView = popDelegate?.productDetailAnimatorPopDelegateFromView()
            transitionContext.containerView.addSubview(imageView!)
            let indexPath = popDelegate?.productDetailAnimatorPopDelegateFromIndexPath()
            
            let fromPopView = pushToDelegate?.productDetailAnimatorPushToDelegateToView()
            fromPopView?.isHidden = true
            
            UIView.animate(withDuration: animationDurantion, animations: {
                fromView.alpha = 0
                imageView?.frame = (self.pushDelegate?.productDetailAnimatorPushDelegatePushFromRect(indexPath: indexPath!))!
            }) { (isFinish) in
                imageView?.removeFromSuperview()
                transitionContext.completeTransition(true)
            }
            break
        case .none:
            break
        }
        
    }
}
