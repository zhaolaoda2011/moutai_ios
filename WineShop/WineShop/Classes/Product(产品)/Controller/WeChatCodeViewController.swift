//
//  WeChatCodeViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/31.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

class WeChatCodeViewController: BaseViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var imageUrl : String?
    
    @IBOutlet weak var codeImageView: UIImageView!
    
    var backBlock : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = imageUrl {
            NetWorkRequestTool.downloadImageToImageView(codeImageView, url)
        }
    }
    
    ///返回
    @IBAction func backAction(_ sender: Any) {
        guard let block = backBlock else {
            return
        }
        block()
        self.dismiss(animated: true) {
            
        }
    }
    
    @objc func image(image:UIImage ,didFinishSavingWithError error : Error? ,contextInfo : Any){
        if error == nil{
            showMessageHUD("保存成功")
        }else{
            showMessageHUD("保存失败:\(error.debugDescription)")
        }
        backButton.alpha = 1
        saveButton.alpha = 1
    }
    
    @IBAction func saveToAlbumAction(_ sender: Any) {
        backButton.alpha = 0
        saveButton.alpha = 0
        
        UIGraphicsBeginImageContextWithOptions(self.view.frame.size, false, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        view.layer.render(in: context!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if let img = image{
            UIImageWriteToSavedPhotosAlbum(img, self, #selector(image(image:didFinishSavingWithError:contextInfo:)), nil)
        }
    }
    
    
}
