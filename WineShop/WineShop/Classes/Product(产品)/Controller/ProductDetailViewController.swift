//
//  ProductDetailViewController.swift
//  WineShop
//
//  Created by zc on 2018/5/15.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit
import SDWebImage
import SafariServices

enum PayType : Int {
    case PayTypeAli = 1
    case PayTypeWechat = 2
}

enum WineOrderStatus : Int {
    //未支付
    case WineOrderStatusNonPayment = 10
    //已支付
    case WineOrderStatusHavePaid = 20
    //已发货
    case WineOrderStatusShipped = 30
    //已收货
    case WineOrderStatusReceived = 40
    //已取消
    case WineOrderStatusCancle = 90
}

class ProductDetailViewController: BaseViewController {
    var indexPath : IndexPath?
    var imageArray = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var pageView: UIView!
    @IBOutlet weak var pageLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var totalLabel: UIButton!
    
    var model : Product = Product()
    var orderId : Int = -1
    var address : AddressInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDetailData()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationBecomeActive), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    @objc func applicationBecomeActive(){
        checkOrderStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if UserInfoTool.checkUserLogin(){
            if self.address == nil{
                getDefaultAddressList()
//                guard let address = UserInfoTool.loadUserInfo().address else{
//                    return
//                }
//                self.addressLabel.text = "\(address.address1 ?? "") \(address.address2 ?? "")"
//                self.address = address
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    ///点击了选择地址
    @IBAction func addressLabelTap(_ sender: Any) {
        if !UserInfoTool.checkUserLogin(){
            let login = SignInViewController()
            self.present(login, animated: true) {
                
            }
            return
        }
        let address = AddressManagerViewController()
        address.selectAddressBlock = {[weak self]info in
            self?.addressLabel.text =  info.address ?? "" //"\(info.address1 ?? "") \(info.address2 ?? "")"
            self?.address = info
        }
        self.navigationController?.pushViewController(address, animated: true)
    }
    
    ///减少
    @IBAction func releaseButtonClick(_ sender: Any) {
        let curCount = Int(countLabel.text ?? "0")!
        let count = curCount - 1 <= 0 ? 0 : curCount - 1
        countLabel.text = "\(count)"
        totalLabel.setTitle("总计:￥\(Float(count)*model.price)", for: UIControlState.normal)
    }
    
    ///增加
    @IBAction func addButtonClick(_ sender: Any) {
        let curCount = Int(countLabel.text ?? "0")!
        let count = curCount + 1
        countLabel.text = "\(count)"
        totalLabel.setTitle("总计:￥\(Float(count)*model.price)", for: UIControlState.normal)
    }
    
    ///微信支付
    @IBAction func webchatPayButtonClick(_ sender: Any) {
        if !UserInfoTool.checkUserLogin(){
            let login = SignInViewController()
            self.present(login, animated: true) {
                
            }
            return
        }
        
        let curCount = Int(countLabel.text ?? "0")!
        if curCount <= 0{
            showMessageHUD("请选择购买数量")
            return
        }
        
        if address == nil {
            showMessageHUD("请选择地址")
            return
        }
        
        let actionSheet = UIAlertController(title: "请选择支付方式", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        let cancel = UIAlertAction(title: "取消", style: UIAlertActionStyle.cancel, handler: nil)
        let alipay = UIAlertAction(title: "支付宝", style: UIAlertActionStyle.default) { [weak self](action) in
            self?.gotoPay(type: PayType.PayTypeAli)
        }
        let webchat = UIAlertAction(title: "微信", style: UIAlertActionStyle.default) { [weak self](action) in
            self?.gotoPay(type: PayType.PayTypeWechat)
        }
        actionSheet.addAction(alipay)
        actionSheet.addAction(webchat)
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true) {
            
        }
        
        //        let result = PayResultViewController()
        //        self.navigationController?.present(result, animated: true, completion: {
        //
        //        })
    }
}

//MARK: UI
extension ProductDetailViewController:UIScrollViewDelegate{
    private func setupUI(){
        self.title = "产品详情"
        self.view.backgroundColor = UIColor.white
        
        //设置头部的frame
        var headFrame = headerView.frame
        headFrame.size.height = addressLabel.frame.maxY + 10
        headerView.frame = headFrame
        tableView.tableHeaderView = headerView
        
        //设置page圆角
        pageView.layer.cornerRadius = pageView.frame.height / 2.0
        pageView.layer.masksToBounds = true
        
        //加入图片
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
        imageArray = [String]()
        //        for image in model.images {
        //            imageArray.append(image.url)
        //        }
        //
        let group = DispatchGroup()
        for image in model.images{
            imageArray.append(image.url)
            group.enter()
            SDWebImageManager.shared().loadImage(with: URL(string: image.url), options: [], progress: { (current, total, url) in
                
            }) { (image, data, error, type, finish, url) in
                group.leave()
            }
        }
        
        group.notify(queue: DispatchQueue.main) {
            for (index,obj) in self.imageArray.enumerated() {
                let x:CGFloat = CGFloat(index)*kScreenWidth()
                let y:CGFloat = 0
                let w:CGFloat = kScreenWidth()
                let h:CGFloat = self.scrollView.frame.height
                let imageView = UIImageView(frame: CGRect(x: x, y: y, width: w, height: h))
                imageView.image = SDWebImageManager.shared().imageCache?.imageFromCache(forKey: obj)
                imageView.contentMode = UIViewContentMode.scaleAspectFit
                self.scrollView.addSubview(imageView)
            }
        }
        
        scrollView.contentSize = CGSize(width: CGFloat(imageArray.count) * kScreenWidth(), height: 0)
        pageLabel.text = "\(imageArray.count > 0 ? "1" : "0")/\(imageArray.count)"
        pageView.isHidden = !(imageArray.count > 1)
        
        priceLabel.text = "￥\(model.price)"
        nameLabel.text = model.name
        
        totalLabel.setTitle("总计:￥0", for: UIControlState.normal)
        
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let index = Int(scrollView.contentOffset.x / kScreenWidth())
        pageLabel.text = "\(index+1)/\(imageArray.count)"
    }
}

//MARK: 导航栏动画
extension ProductDetailViewController:ProductDetailAnimatorPushToDelegate,ProductDetailAnimatorPopDelegate{
    func productDetailAnimatorPopDelegateFromIndexPath() -> IndexPath {
        return indexPath!
    }
    
    func productDetailAnimatorPopDelegateFromView() -> UIImageView {
        scrollView.setContentOffset(CGPoint.zero, animated: false)
        tableView.setContentOffset(CGPoint.zero, animated: false)
        
        let maxW = kScreenWidth()
        let maxHeight = maxW * 325.0 / 375.0
        
        
        var x : CGFloat = 0
        var y : CGFloat = 0
        var w : CGFloat = 0
        var h : CGFloat = 0
        
        let imageView = UIImageView()
        
        if let image = SDWebImageManager.shared().imageCache?.imageFromCache(forKey: model.url){
            if image.size.width > image.size.height{
                x = 0
                w = maxW
                h = w / image.size.width * image.size.height
                y = (maxHeight - h)/2.0+64
            }else{
                y = 0+64
                h = maxHeight
                w = h / image.size.height * image.size.width
                x = (maxW-w)/2.0
            }
            imageView.image = image
        }
        
        
        imageView.frame = CGRect(x: x, y: y, width: w, height: h)
        
        return imageView
    }
    
    func productDetailAnimatorPushToDelegateToView() -> UIView {
        return scrollView
    }
}

//MARK: uitableview delegate datasource
extension ProductDetailViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        return cell!
    }
}

//MARK: 网络请求
extension ProductDetailViewController{
    private func getDetailData(){
        let param = ["id":model.id]
        NetWorkRequestTool.request(url: productitem, methodType: .get, parameters: param) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard var info = Product.deserialize(from: data) else{
                    return
                }
                info.url = self?.model.url ?? ""
                self?.model = info
                self?.setupUI()
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
        
    }
    
    //检测订单 状态
    private func checkOrderStatus(){
        if orderId <= 0{
            return
        }
        let param = ["id":orderId]
        showLoadingHUD("")
        NetWorkRequestTool.request(url: orderitem, methodType: .get, parameters: param) { (result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let m = OrderInfo.deserialize(from: data) else{
                    return
                }
                if let s = WineOrderStatus.init(rawValue: m.status){
                    switch s{
                    case .WineOrderStatusHavePaid:
                        showMessageHUD("支付成功")
                        break
                    default :
                        showMessageHUD("未支付")
                        break
                    }
                }else{
                    showMessageHUD("订单状态有误")
                }
                
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
    ///支付
    private func gotoPay(type : PayType){
        
        let curCount = Int(countLabel.text ?? "0")!
        if curCount <= 0{
            showMessageHUD("请选择购买数量")
            return
        }
        showLoadingHUD("正在生成订单...")
        let param = ["product_id":"\(model.id)","amount":"\(curCount)","pay_type" : "\(type.rawValue)","address_id":"\(address!.id)"]
        NetWorkRequestTool.request(url: ordersave, methodType: .post, parameters: param) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String:Any] else{
                    return
                }
                guard let m = PrePayOrderInfo.deserialize(from: data) else{
                    return
                }
                
                switch type{
                case .PayTypeAli:
                    let scheme = "alipays://platformapi/startapp?saId=10000007&qrcode=\(m.code_url!)?_s=web-other"
                    if let url = URL(string: scheme){
                        if UIApplication.shared.canOpenURL(url){
                            UIApplication.shared.openURL(url)
                        }else{
                            showMessageHUD("没有安装支付宝")
                        }
                    }
                    break
                case .PayTypeWechat:
//                    let scheme = m.code_url!
//                    if let url = URL(string: scheme){
//                        UIApplication.shared.openURL(url)
//                    }
                    let code = WeChatCodeViewController()
                    code.imageUrl = m.code_img_url
                    code.backBlock = {[weak self] in
                        self?.checkOrderStatus()
                    }
                    self?.present(code, animated: true, completion: {
                        
                    })
                    break
                }
                
                self?.orderId = m.id
                
                //                SwiftFintechClient.sharedInstance().fintech(m.token_id, fintechServicesString: SwiftFintechClientConstAppFintechConfig.getType1appKey(), finish: { (result) in
                //                    let payState = SPClientConstEnumFintechState.init(result!["pState"] as! UInt32)
                //                    let messageStr = result!["messageString"] as? String
                //                    switch payState{
                //                    case SPClientConstEnumFintechSuccess:
                //                        break
                //                    default:
                //                        showMessageHUD("支付失败，错误号:\(payState),错误原因:\(messageStr ?? "")")
                //                        break
                //                    }
                //                })
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
    
    ///获取默认地址
    private func getDefaultAddressList(){
        NetWorkRequestTool.request(url: addresslist, methodType: .get, parameters: [:]) { [weak self](result, error) in
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [Any] else{
                    return
                }
                guard let list = [AddressInfo].deserialize(from: data) else{
                    return
                }
                list.forEach({ (model) in
                    guard let m = model else{
                        return
                    }
                    if m.is_default == true{
                        self?.address = m
                        DistrictManager.shareInstance.getAddressByCode(m.province_id, m.city_id, m.district_id) { [weak self](str) in
                            self?.addressLabel.text = "\(str) \(m.address ?? "")"//"\(m.address1 ?? "")\(m.address2 ?? "")"
                        }
                    }
                })

            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
    }
}
