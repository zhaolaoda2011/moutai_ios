//
//  UserInfo.swift
//  Album
//
//  Created by zc on 2018/1/10.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation
import HandyJSON

class UserInfo: NSObject, HandyJSON,NSCoding {
    var token : String?
    var id : String?
    var avatar_url : String?
    var username : String?
    var mobile : String?
    
    var sex : Int = 0
    var id_card : Int = 0
    var balance : Int = 0
    var bonus : Int = 0
    var fx_role : Int = 0
    var child_amount_first : Int = 0
    var child_amount_second : Int = 0
    var status : Int = 0
    
    var address : AddressInfo?
//    var address_id : Int = 0
//    var address1 : Int = 0
//    var address2 : Int = 0
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(token, forKey: "token")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(avatar_url, forKey: "avatar_url")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(mobile, forKey: "mobile")

        aCoder.encode(sex, forKey: "sex")
        aCoder.encode(id_card, forKey: "id_card")
        aCoder.encode(balance, forKey: "balance")
        aCoder.encode(fx_role, forKey: "fx_role")
        aCoder.encode(child_amount_first, forKey: "child_amount_first")
        aCoder.encode(child_amount_second, forKey: "child_amount_second")
        aCoder.encode(status, forKey: "status")
        
        aCoder.encode(address, forKey: "address")
    }
    
    required init?(coder aDecoder: NSCoder) {
        token = aDecoder.decodeObject(forKey: "token") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        avatar_url = aDecoder.decodeObject(forKey: "avatar_url") as? String
        username = aDecoder.decodeObject(forKey: "username") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String

        sex = aDecoder.decodeInteger(forKey: "sex")
        id_card = aDecoder.decodeInteger(forKey: "id_card")
        balance = aDecoder.decodeInteger(forKey: "balance")
        bonus = aDecoder.decodeInteger(forKey: "bonus")
        fx_role = aDecoder.decodeInteger(forKey: "fx_role")
        child_amount_first  = aDecoder.decodeInteger(forKey: "child_amount_first")
        child_amount_second  = aDecoder.decodeInteger(forKey: "child_amount_second")
        status  = aDecoder.decodeInteger(forKey: "status")
        
        address  = aDecoder.decodeObject(forKey: "address") as? AddressInfo
    }
    
    required override init() {
    }
}
