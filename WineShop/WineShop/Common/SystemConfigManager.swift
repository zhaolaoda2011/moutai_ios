//
//  SystemConfigManager.swift
//  WineShop
//
//  Created by zc on 2018/5/18.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit
import HandyJSON

struct SystemConfig : HandyJSON {
    var user_status = [String:String]()
    var earn_log_type = [String:String]()
    var fx_level = [String:String]()
    var fx_role = [String]()
    var earn_type = [String:String]()
    var order_status = [String:String]()
    var withdraw_deposit_status = [String:String]()
    var is_prod : Bool = false
}

class SystemConfigManager: NSObject {
    static let shareInstance = SystemConfigManager()
    ///用于存储在本地的key
    static let kSystemConfigKey = "kSystemConfigKey"
    
    var systemConfigModel = SystemConfig()
    
    func setupSystemConfigData(){
        //        if !checkSystemConfigData(){
        showLoadingHUD("正在初始化...")
        NetWorkRequestTool.request(url: systemConfig, methodType: .get, parameters: [:]) { [weak self](result, error) in
            hideAllHUD()
            if NetWorkRequestTool.validateRequestRespone(result){
                guard let data = result!["data"] as? [String : Any] else{
                    return
                }
                guard let config = SystemConfig.deserialize(from: data) else{
                    return
                }
                self?.systemConfigModel = config
                
                self?.setupAllUIByIs_prod()
                
                let json = config.toJSONString()
                UserDefaults.standard.set(json, forKey: SystemConfigManager.kSystemConfigKey)
                UserDefaults.standard.synchronize()
                
            }else{
                NetWorkRequestTool.showRequestMessage(result)
            }
        }
        //        }
    }
    
    /// 检查是否缓存了config数据
    func checkSystemConfigData()->Bool{
        guard let data = UserDefaults.standard.value(forKey: SystemConfigManager.kSystemConfigKey) else{
            return false
        }
        guard let cityJson = data as? String else{
            return false
        }
        
        if !cityJson.isEmpty(){
            if let config = SystemConfig.deserialize(from: cityJson){
                self.systemConfigModel = config
            }
        }
        
        return !(cityJson.isEmpty || cityJson == "")
    }
    
    
    func setupAllUIByIs_prod(){
        if let ctr = UIApplication.shared.keyWindow?.rootViewController{
            if let tabbar = ctr as? HomeTabbarController{
//                self.systemConfigModel.is_prod = true
                tabbar.setupController()
            }
        }
    }
}
