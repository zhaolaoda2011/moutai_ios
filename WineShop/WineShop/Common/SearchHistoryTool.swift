//
//  SearchHistoryTool.swift
//  juzhuanwang
//
//  Created by zc on 2018/3/23.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

enum SearchHistoryToolType : Int {
    case SearchHistoryToolTypeCompany = 0
    case SearchHistoryToolTypeJinDiao
}

class SearchHistoryTool: NSObject {
    static let documentDirectoryPath = "\(NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!)/userSearch.data"
    static let shareInstance = SearchHistoryTool()
    private override init() {
    }
    
    func saveHistroyWithType(type : SearchHistoryToolType,searchStr : String){
        if var data = NSKeyedUnarchiver.unarchiveObject(withFile: SearchHistoryTool.documentDirectoryPath) as? [String : Any]{
            if var array = data["\(type.rawValue)"] as? [String]{
                if !array.contains(searchStr){
                    array.insert(searchStr, at: 0)
                }
                data["\(type.rawValue)"] = array
                NSKeyedArchiver.archiveRootObject(data, toFile: SearchHistoryTool.documentDirectoryPath)
            }else{
                data["\(type.rawValue)"] = [searchStr]
                NSKeyedArchiver.archiveRootObject(data, toFile: SearchHistoryTool.documentDirectoryPath)
            }
        }else{
            let dic = ["\(type.rawValue)" : [searchStr]]
            NSKeyedArchiver.archiveRootObject(dic, toFile: SearchHistoryTool.documentDirectoryPath)
        }
    }
    
    func loadHistroyWithType(type : SearchHistoryToolType)->[String]{
        if let data = NSKeyedUnarchiver.unarchiveObject(withFile: SearchHistoryTool.documentDirectoryPath) as? [String : Any]{
            if let array = data["\(type.rawValue)"] as? [String]{
                return array
            }
        }
        return [String]()
    }
    
    func clearHistoryWithType(type : SearchHistoryToolType){
        if var data = NSKeyedUnarchiver.unarchiveObject(withFile: SearchHistoryTool.documentDirectoryPath) as? [String : Any]{
            data["\(type.rawValue)"] = [String]()
            NSKeyedArchiver.archiveRootObject(data, toFile: SearchHistoryTool.documentDirectoryPath)
        }
    }
}
