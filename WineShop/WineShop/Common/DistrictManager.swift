//
//  DistrictManager.swift
//  juzhuanwang
//
//  Created by apple on 2018/2/17.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation
import HandyJSON

struct DistrictModel : HandyJSON {
    var code : Int = 0
    var name : String?
    var pid : Int = 0
    var isSelect : Bool = false
    var child : [DistrictModel] = [DistrictModel]()
}



class DistrictManager : NSObject {
    static let shareInstance = DistrictManager()
    ///用于存储在本地的key 没有带全部选项的数据
    static let kCityKey = "kCityKey"
    ///用于存储在本地的key 有全部选项的数据
    static let kCityWithAllKey = "kCityWithAllKey"
    ///用于存储在本地的key 字典类型  传入对应的code就能 获取省市区名字
    static let kFastGetCityKey = "kFastGetCityKey"

    /// 省市区数组
    var districtArray = [DistrictModel]()
    /// 存储省市区 快速查询字典
    var districtDict = [String:String]()
    ///多线程
    let queue = DispatchQueue(label: "DistrictManager")
    
    /// 初始化省市区
    func setupDistrictData(_ dataBlock : ((_ districtArray : [DistrictModel]?)->())?){
        queue.async {
            if !self.checkDistrictData(){
                NetWorkRequestTool.request(url: systemDistrict, methodType: .get, parameters: nil) { [weak self](result, error) in
                    if NetWorkRequestTool.validateRequestRespone(result){
                        DispatchQueue(label: "DistrictManagerSort").async {
                            guard let datas = [DistrictModel].deserialize(from: result!["data"] as? [Any]) else{
                                return
                            }
                            
                            ///用于快速查询城市
                            var allModel = [String:String]()
                            
                            //带有全部这个选项的数据
                            var dataWithAll = datas as! [DistrictModel]
                            for (index,obj) in dataWithAll.enumerated(){
                                var provice = obj
                                allModel["\(provice.code)"] = provice.name
                                for (cindex,city) in provice.child.enumerated(){
                                    var c = city
                                    allModel["\(c.code)"] = c.name
                                    let allDistrict = DistrictModel(code: -1, name: "全部", pid: 0, isSelect: false, child: [DistrictModel]())
                                    c.child.insert(allDistrict, at: 0)
                                    provice.child[cindex] = c
                                    for d in c.child{
                                        allModel["\(d.code)"] = d.name
                                    }
                                }
                                let allCity = DistrictModel(code: -1, name: "全部", pid: 0, isSelect: false, child: [DistrictModel]())
                                provice.child.insert(allCity, at: 0)
                                dataWithAll[index] = provice
                            }
                            
                            let allProvince = DistrictModel(code: -1, name: "全部", pid: 0, isSelect: false, child: [DistrictModel]())
                            dataWithAll.insert(allProvince, at: 0)
                            
                            self?.districtArray = datas as! [DistrictModel]
                            
                            ///保存本地
                            let cityJson = (datas as! [DistrictModel]).toJSONString()
                            UserDefaults.standard.set(cityJson, forKey: DistrictManager.kCityKey)
                            UserDefaults.standard.set(dataWithAll.toJSONString(), forKey: DistrictManager.kCityWithAllKey)
                            UserDefaults.standard.set(allModel, forKey: DistrictManager.kFastGetCityKey)
                            
                            self?.districtDict = allModel
                            
                            guard let successBlock = dataBlock else{
                                return
                            }
                            successBlock(self?.districtArray)
                        }
                    }else{
                        NetWorkRequestTool.showRequestMessage(result)
                    }
                }
            }
        }

    }
    
    ///获取城市数据的json
    func getDistrictDataJson()->String?{
        guard let data = UserDefaults.standard.value(forKey: DistrictManager.kCityKey) else{
            return nil
        }
        guard let cityJson = data as? String else{
            return nil
        }
        return cityJson
    }
    
    ///获取城市数据的json
    func getDistrictWithAllDataJson()->String?{
        guard let data = UserDefaults.standard.value(forKey: DistrictManager.kCityWithAllKey) else{
            return nil
        }
        guard let cityJson = data as? String else{
            return nil
        }
        return cityJson
    }
    
    /// 检查是否缓存了城市数据
    func checkDistrictData()->Bool{
        guard let data = UserDefaults.standard.value(forKey: DistrictManager.kCityKey) else{
            return false
        }
        guard let cityJson = data as? String else{
            return false
        }
        
//        guard let array = [DistrictModel].deserialize(from: cityJson) else{
//            return false
//        }
//        self.districtArray = array as! [DistrictModel]
        
        if let data = UserDefaults.standard.value(forKey: DistrictManager.kFastGetCityKey){
            if let dic = data as? [String:String] {
                self.districtDict = dic
            }
        }
        
        return !(cityJson.isEmpty || cityJson == "")
    }
    
    /// 检查是否缓存了快速查询的城市数据
    func checkFastDistrictData()->Bool{
        guard let data = UserDefaults.standard.value(forKey: DistrictManager.kFastGetCityKey) else{
            return false
        }
        guard let cityJson = data as? [String:String] else{
            return false
        }
        return cityJson != [:]
    }
    
    /// 检查是否缓存了带有全部选项的城市数据
    func checkDistrictWithAllData()->Bool{
        guard let data = UserDefaults.standard.value(forKey: DistrictManager.kCityWithAllKey) else{
            return false
        }
        guard let cityJson = data as? [String:String] else{
            return false
        }
        return cityJson != [:]
    }
    
    //获取带有全部选项的城市数据
    func getDistrictWithAllData(_ block : ((_ array : [String:String])->())?){
        if checkDistrictWithAllData(){
            guard let data = UserDefaults.standard.value(forKey: DistrictManager.kCityWithAllKey) else{
                return
            }
            guard let dic = data as? [String:String] else{
                return
            }
            guard let successBlock = block else{
                return
            }
            successBlock(dic)
        }else{
            getDistrictData({ (array) in
                guard let data = UserDefaults.standard.value(forKey: DistrictManager.kCityWithAllKey) else{
                    return
                }
                guard let dic = data as? [String:String] else{
                    return
                }
                guard let successBlock = block else{
                    return
                }
                successBlock(dic)
            })
        }
    }
    
    
    //获取城市数据字典
    func getFastDistrictData(_ block : ((_ array : [String:String])->())?){
        if checkFastDistrictData(){
            guard let data = UserDefaults.standard.value(forKey: DistrictManager.kFastGetCityKey) else{
                return
            }
            guard let dic = data as? [String:String] else{
                return
            }
            guard let successBlock = block else{
                return
            }
            successBlock(dic)
        }else{
            getDistrictData({ (array) in
                guard let data = UserDefaults.standard.value(forKey: DistrictManager.kFastGetCityKey) else{
                    return
                }
                guard let dic = data as? [String:String] else{
                    return
                }
                guard let successBlock = block else{
                    return
                }
                successBlock(dic)
            })
        }
    }
    
    /// 获取省市区数据
    func getDistrictData(_ block : ((_ array : [DistrictModel])->())?){
        if checkDistrictData() {
            guard let data = UserDefaults.standard.value(forKey: DistrictManager.kCityKey) else{
                return
            }
            guard let cityJson = data as? String else{
                return
            }
            guard let successBlock = block else{
                return
            }
            guard let array = [DistrictModel].deserialize(from: cityJson) else{
                return
            }
            successBlock(array as! [DistrictModel])
//            successBlock(self.districtArray)
        }else{
            setupDistrictData({ (array) in
                guard let successBlock = block else{
                    return
                }
                
                successBlock(array!)
            })
        }
    }
    
    ///根据id获取名字
    func getName(_ id:Int,_ block : ((_ address : String)->())?){
        if checkFastDistrictData(){
            guard let data = UserDefaults.standard.value(forKey: DistrictManager.kFastGetCityKey) else{
                return
            }
            guard let dic = data as? [String:String] else{
                return
            }
            guard let successBlock = block else{
                return
            }
            let str = "\(dic["\(id)"] ?? "")"
            successBlock(str)
        }else{
            getDistrictData({ (array) in
                guard let data = UserDefaults.standard.value(forKey: DistrictManager.kFastGetCityKey) else{
                    return
                }
                guard let dic = data as? [String:String] else{
                    return
                }
                guard let successBlock = block else{
                    return
                }
                let str = "\(dic["\(id)"] ?? "")"
                successBlock(str)
            })
        }
//        getDistrictData { (array) in
//            guard let successBlock = block else{
//                return
//            }
//            for province in array{
//                if id == province.code{
//                    successBlock(province.name!)
//                }
//                for city in province.childs{
//                    if id == city.code{
//                        successBlock(city.name!)
//                    }
//                    for dist in city.childs{
//                        if id == dist.code{
//                            successBlock(dist.name!)
//                        }
//                    }
//                }
//            }
//        }
    }
    
    ///根据省市区id获得地址
    func getAddressByCode(_ province_id:Int,_ city_id:Int,_ district_id:Int,_ block : ((_ address : String)->())?){
//        if checkFastDistrictData(){
//            guard let data = UserDefaults.standard.value(forKey: DistrictManager.kFastGetCityKey) else{
//                return
//            }
//            guard let dic = data as? [String:String] else{
//                return
//            }
            guard let successBlock = block else{
                return
            }
            let str = "\(self.districtDict["\(province_id)"] ?? "") \(self.districtDict["\(city_id)"] ?? "") \(self.districtDict["\(district_id)"] ?? "")"
            successBlock(str)
//        }else{
//            setupDistrictData({ (array) in
//                guard let data = UserDefaults.standard.value(forKey: DistrictManager.kFastGetCityKey) else{
//                    return
//                }
//                guard let dic = data as? [String:String] else{
//                    return
//                }
//                guard let successBlock = block else{
//                    return
//                }
//                let str = "\(dic["\(province_id)"] ?? "") \(dic["\(city_id)"] ?? "") \(dic["\(district_id)"] ?? "")"
//                successBlock(str)
//            })
//        }
//        //queue.async {
//            if self.checkDistrictData() {
//                guard let data = UserDefaults.standard.value(forKey: DistrictManager.kCityKey) else{
//                    return
//                }
//                guard let cityJson = data as? String else{
//                    return
//                }
//                guard let successBlock = block else{
//                    return
//                }
//                guard let array = [DistrictModel].deserialize(from: cityJson) else{
//                    return
//                }
//                var provinceStr = ""
//                var cityStr = ""
//                var distinctStr = ""
//                for province in array{
//                    guard let pro = province else{
//                        return
//                    }
//                    if pro.code == province_id{
//                        provinceStr = pro.name!
//                    }
//                    for city in pro.childs{
//                        if city.code == city_id{
//                            cityStr = city.name!
//                        }
//                        for distinct in city.childs{
//                            if distinct.code == district_id{
//                                distinctStr = distinct.name!
//                            }
//                        }
//                    }
//                }
//                successBlock("\(provinceStr) \(cityStr) \(distinctStr)")
//            }else{
//                self.setupDistrictData({ (result) in
//                    guard let successBlock = block else{
//                        return
//                    }
//                    var provinceStr = ""
//                    var cityStr = ""
//                    var distinctStr = ""
//                    guard let array = result else{
//                        return
//                    }
//                    for province in array{
//                        let pro = province
//                        if pro.code == province_id{
//                            provinceStr = pro.name!
//                        }
//                        for city in pro.childs{
//                            if city.code == city_id{
//                                cityStr = city.name!
//                            }
//                            for distinct in city.childs{
//                                if distinct.code == district_id{
//                                    distinctStr = distinct.name!
//                                }
//                            }
//                        }
//                    }
//                    successBlock("\(provinceStr) \(cityStr) \(distinctStr)")
//                })
//            }
//        //}
        
    }
}


