//
//  CommonColor.swift
//  Album
//
//  Created by zc on 2017/11/16.
//  Copyright © 2017年 zj. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import MBProgressHUD

// MARK: - 颜色 字体
///controller 背景颜色
let kControllerBGColor = UIColor.withHex(hexInt: 0xFAFAFA)
///主体颜色
let kMainColor = UIColor.withHex(hexInt: 0x5191FF)
///字体颜色
let kFontColor = UIColor.withHex(hexInt: 0x333333)
///弹框颜色
let kPopWindowBGColor = UIColor.withHex(hexInt: 0x2E71A7)
///分割线颜色
let kSeperatorLineColor = UIColor.withHex(hexInt: 0xE0E0E0)//UIColor.withRGB(224, 224, 224)
///普通字体
func kFontWithSize(size : CGFloat)->(UIFont){
    return UIFont.init(name: "PingFangSC-Regular", size: size)!//UIFont.systemFont(ofSize: size)
}
///加粗字体
func kFontBlodWithSize(size : CGFloat)->(UIFont){
    return UIFont.init(name: "PingFangSC-Medium", size: size)!//UIFont.systemFont(ofSize: size, weight: UIFont.Weight.bold)
}
///细字体
func kFontThinWithSize(size : CGFloat)->(UIFont){
    return UIFont.systemFont(ofSize: size, weight: UIFont.Weight.light)
}

// MARK: - 屏幕宽度 高度
///屏幕宽度
func kScreenWidth()->(CGFloat){
    let kScreenWidth = UIScreen.main.bounds.width
    return kScreenWidth
}
///屏幕高度
func kScreenHeight()->(CGFloat){
    let kScreenHeight = UIScreen.main.bounds.height
    return kScreenHeight
    
}

///状态栏高度
func kStatusBarHeight () -> (CGFloat) {
    let rectStatus : CGRect = UIApplication.shared.statusBarFrame
    return rectStatus.size.height
}

///导航栏高度
func kNavigationBarHeight() -> (CGFloat) {
    let rectNav : CGRect = UINavigationBar.appearance().bounds
    return rectNav.size.height
}


// MARK: - window相关
func viewOfKeyWindows()->(UIView){
    let rootCtr = UIApplication.shared.keyWindow
    let windowView = rootCtr!
    return windowView
}


// MARK: - 状态栏
///设置状态栏背景颜色
func setStatusBarBackgroundColor(color : UIColor) {
    let statusBarWindow : UIView = UIApplication.shared.value(forKey: "statusBarWindow") as! UIView
    let statusBar : UIView = statusBarWindow.value(forKey: "statusBar") as! UIView
    /*
     if statusBar.responds(to:Selector("setBackgroundColor:")) {
     statusBar.backgroundColor = color
     }*/
    if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
        statusBar.backgroundColor = color
    }
}


// MARK: - 弹框
/////显示纯文字的弹框 在windows上显示
//func showMessageHUD(_ text : String){
//    let mb = MBProgressHUD.showAdded(to: viewOfKeyWindows(), animated: true)
//    mb?.mode = MBProgressHUDModeText
//    mb?.labelText = text
//    mb?.labelFont = kFontWithSize(size: 15)
//    mb?.detailsLabelFont = kFontWithSize(size: 15)
//    mb?.hide(true, afterDelay: 2)
//}
/////显示文字弹框  在指定的view
//func showMessageHUD(_ text : String,_ inView : UIView){
//    let mb = MBProgressHUD.showAdded(to: inView, animated: true)
//    mb?.mode = MBProgressHUDModeText
//    mb?.labelText = text
//    mb?.labelFont = kFontWithSize(size: 15)
//    mb?.detailsLabelFont = kFontWithSize(size: 15)
//    mb?.hide(true, afterDelay: 2)
//}
//
/////显示菊花带字的弹框 不传文字 就不显示文字
//func showLoadingHUD(_ text : String){
//    let mb = MBProgressHUD.showAdded(to: viewOfKeyWindows(), animated: true)
//    mb?.mode = MBProgressHUDModeIndeterminate
//    mb?.labelFont = kFontWithSize(size: 15)
//    mb?.detailsLabelFont = kFontWithSize(size: 15)
//    mb?.labelText = text
//}
//func showLoadingHUD(_ text : String,_ inView:UIView){
//    let mb = MBProgressHUD.showAdded(to: inView, animated: true)
//    mb?.mode = MBProgressHUDModeIndeterminate
//    mb?.labelFont = kFontWithSize(size: 15)
//    mb?.detailsLabelFont = kFontWithSize(size: 15)
//    mb?.labelText = text
//}
/////关闭window所有弹框
//func hideAllHUD(){
//    MBProgressHUD.hideAllHUDs(for: viewOfKeyWindows(), animated: true)
//}
/////关闭指定view的HUD
//func hideAllHUD(inView : UIView){
//    MBProgressHUD.hide(for: inView, animated: true)
//}
///显示纯文字的弹框
func showMessageHUD(_ text : String){

    let hud = MBProgressHUD.showAdded(to:viewOfKeyWindows(), animated: true)
    hud.mode = MBProgressHUDMode.text

    hud.label.text = text
    hud.label.numberOfLines = 0
    hud.contentColor = UIColor.white
    hud.bezelView.color = UIColor.black
    hud.hide(animated: true, afterDelay: 1.0)

}

///显示菊花带字的弹框 不传文字 就不显示文字
func showLoadingHUD(_ text : String){
    let hud = MBProgressHUD.showAdded(to:viewOfKeyWindows(), animated: true)
    if text.isEmpty{
        hud.contentColor = UIColor.white
        hud.backgroundView.style = MBProgressHUDBackgroundStyle.solidColor
        hud.backgroundView.color = UIColor.init(white: 0.1, alpha: 0)
        hud.bezelView.color = UIColor.black
    }else{
        hud.label.text = text
        hud.contentColor = UIColor.white
        hud.backgroundView.style = MBProgressHUDBackgroundStyle.solidColor
        hud.backgroundView.color = UIColor.init(white: 0.1, alpha: 0)
        hud.bezelView.color = UIColor.black
    }
}
///关闭所有弹框
func hideAllHUD(){
    MBProgressHUD.hide(for: viewOfKeyWindows(), animated: true)
}

// MARK: 打电话
func callByPhoneNuber(_ phoneNumber : String){
    //2.有提示 telprompt
    UIApplication.shared.openURL(URL(string: "tel://\(phoneNumber)")!)
//    //1.有提示
//    let callWebView = UIWebView()
//    callWebView.loadRequest(URLRequest(url:URL(string: "tel:\(10086)")!))
//    self.addSubview(callWebView)
}

// MARK: 打印
//自定义打印日志
func printM<T>(_ message : T,file : String = #file,funcName : String = #function,lineNum : Int = #line){
    //bulid setting -> 搜索 swift flag -> debug 填写-D DEBUG(在DEBUG模式下添加一个DEBUG的标记)
    #if DEBUG
    let f = (file as NSString).lastPathComponent
    let funcName = funcName
    let lineNum = lineNum
    print("--\(f):[\(funcName)](Line:\(lineNum)):\r\n\(message)")
    #endif
}

////MARK: 正则匹配用户身份证号15或18位
//func checkUserIdCard(idCard:String) ->Bool {
//    guard idCard.count >= 15 else {return false}
//    let pattern = "(^[0-9]{15}$)|([0-9]{17}([0-9]|X)$)";
//    let pred = NSPredicate(format: "SELF MATCHES %@", pattern)
//    let isMatch:Bool = pred.evaluate(with: idCard)
//    return isMatch;
//}
