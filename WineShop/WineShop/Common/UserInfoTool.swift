//
//  UserInfoTool.swift
//  Album
//
//  Created by zc on 2018/1/10.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation

class UserInfoTool: NSObject {
    static let documentDirectoryPath = "\(NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!)/userAccount.data"
    
    /// 获取用户信息
    ///
    /// - Returns: 用户信息模型
    class func loadUserInfo()->(UserInfo){
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: UserInfoTool.documentDirectoryPath) else{
            return UserInfo()
        }
        guard let userInfo = data as? UserInfo else{
            return UserInfo()
        }
        return userInfo
    }
    
    /// 保存用户信息
    ///
    /// - Parameter user: 用户信息model
    class func saveUserInfo(user : UserInfo?){
        guard let info = user else {
            return
        }
        NSKeyedArchiver.archiveRootObject(info, toFile: UserInfoTool.documentDirectoryPath)
    }
    
    /// 清除用户信息
    class func clearUserInfo(){
        NSKeyedArchiver.archiveRootObject(UserInfo(), toFile: UserInfoTool.documentDirectoryPath)
    }
    
    /// 检查是否登录
    ///
    /// - Returns: 是否登录
    class func checkUserLogin()->(Bool){
        guard let data = NSKeyedUnarchiver.unarchiveObject(withFile: UserInfoTool.documentDirectoryPath) else{
//            showMessageHUD("检测用户是否登录-读取沙盒数据错误-\(NSKeyedUnarchiver.unarchiveObject(withFile: UserInfoTool.documentDirectoryPath))")
            return false
        }
        guard let userInfo = data as? UserInfo else{
//            showMessageHUD("检测用户是否登录-将数据转成UserInfo失败-\(data as? UserInfo)")
            return false
        }
        guard let token = userInfo.token else {
            //showMessageHUD("检测用户是否登录-token有问题-\(userInfo.token)")
            return false
        }
        return !token.isEmpty
    }
}
