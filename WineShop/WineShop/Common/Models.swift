//
//  Models.swift
//  Album
//
//  Created by zc on 2018/1/10.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation
import HandyJSON

///生成订单信息
struct PrePayOrderInfo:HandyJSON {
    var token_id : String? = ""
    var pay_info : String? = ""//ayInfo?
    var out_trade_no : String? = ""
    var transaction_id : String? = ""
    var service : String? = ""
    var code_img_url : String? = ""
    var code_url : String? = ""
    var code_status : String? = ""
    var type : String? = ""
    var id : Int = -1
}

struct PayInfo:HandyJSON {
    var id : Int = 0
    var appid : String? = ""
    var mch_id : String? = ""
    var partnerid : String? = ""
    var noncestr : String? = ""
    var prepayid : String? = ""
    var result_code : String? = ""
    var return_code : String? = ""
    var return_msg : String? = ""
    var sign : String? = ""
    var trade_type : String? = ""
    var package : String? = ""
    var nonceStr : String? = ""
    var timestamp : UInt32 = 0
}

struct OrderInfo:HandyJSON {
    var id : Int = 0
    var product_id : Int = 0
    var name : String = ""
    var img_url : String = ""
    var amount  : String = ""
    var total_price  : String = ""
    var pay_price : String = ""
    var status : Int = 0
    var create_time : String = ""
}

//MARK: 产品
struct Product:HandyJSON {
    var id : Int = 0
    var name : String = ""
    var url : String = ""
    var price : Float = 0.0
    var images : [ProductImage] = [ProductImage]()
}

//MARK: 产品图片
struct ProductImage : HandyJSON {
    var id : Int = 0
    var item_id  : Int = 0
    var url : String = ""
}

//MARK: 分销总数
struct FxInfo:HandyJSON {
    var first : String = ""
    var second : String = ""
    var first_earn : Float = 0.0
    var second_earn : Float = 0.0
}

//MARK: 成员信息
struct MemberInfo:HandyJSON {
    var id : Int = 0
    var username : String = ""
    var mobile : String = ""
    var avatar_url : String = ""
}

//MARK: 收益
struct WithdrawInfo:HandyJSON {
    var id : Int = 0
    var order_id : Int = 0
    var bonus : String = ""
    var type : Int = 0
    var create_time : String = ""
}

//MARK: 地址
class AddressInfo:NSObject, HandyJSON,NSCoding {
    var id : Int = -1
    var province_id : Int = -1
    var city_id : Int = -1
    var district_id : Int = -1
    var address : String?
//    var address1 : String?
//    var address2 : String?
    var is_default : Bool = false
    var name : String?
    var mobile : String?
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(province_id, forKey: "province_id")
        aCoder.encode(city_id, forKey: "city_id")
        aCoder.encode(district_id, forKey: "district_id")
//        aCoder.encode(address1, forKey: "address1")
//        aCoder.encode(address2, forKey: "address2")
        aCoder.encode(is_default, forKey: "address2")
        aCoder.encode(name, forKey: "address2")
        aCoder.encode(mobile, forKey: "address2")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeInteger(forKey: "id")
        province_id = aDecoder.decodeInteger(forKey: "province_id")
        city_id = aDecoder.decodeInteger(forKey: "city_id")
        district_id = aDecoder.decodeInteger(forKey: "district_id")
//        address1 = aDecoder.decodeObject(forKey: "address1") as? String
//        address2 = aDecoder.decodeObject(forKey: "address1") as? String
        
        is_default = aDecoder.decodeBool(forKey: "is_default")
        name = aDecoder.decodeObject(forKey: "name") as? String
        mobile = aDecoder.decodeObject(forKey: "mobile") as? String
    }
    
    required override init() {
    }
}

//MARK: 成员信息
struct CardInfo:HandyJSON {
    var id : Int = -1
    var card_num : String = ""
    var bank_name : String = ""
    var account_name : String = ""
    var is_default : Bool = false
    var create_time : String = ""
    var allow_fee : String = ""
    var card_type : String = ""
}

