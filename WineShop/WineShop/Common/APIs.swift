//
//  APIs.swift
//  Album
//
//  Created by zc on 2018/1/10.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation

/// 系统参数
let systemParams = generateApiUrl("system/params")

/// 获取常变系统参数和配置
let systemConfig = generateApiUrl("system/config")

/// 省市区
let systemDistrict = generateApiUrl("system/districtOrder")

/// 图片上传
let imgUpload = generateApiUrl("system/imgUpload")

/// 更新我的信息
let accountupdate = generateApiUrl("account/update")

/// 产品列表
let productlist = generateApiUrl("product/list")

/// 产品详情
let productitem = generateApiUrl("product/item")

/// 注册
let accountRegister = generateApiUrl("account/register")

/// 获取验证码
let accountVerifyCode = generateApiUrl("account/verifyCode")

/// 登录
let accountLogin = generateApiUrl("/account/login")

/// 获取个人详细信息
let accountProfile = generateApiUrl("/account/profile")

/// 用户登出
let accountLogout = generateApiUrl("account/logout")

/// 获取用户银行卡列表
let bank_cardList = generateApiUrl("bank_card/list")

/// 添加银行卡
let bank_cardsave = generateApiUrl("bank_card/save")

/// 获取用户地址列表
let addresslist = generateApiUrl("address/list")

/// 添加/更新收货地址 更新需填写条目ID eg:(id=1)
let addresssave = generateApiUrl("address/save")

/// 获取分销总揽(一级分销总数, 二级分销总数)
let fxinfo = generateApiUrl("fx/info")

/// 分销收益列表
let fxearnList = generateApiUrl("fx/earnList")

/// 分销列表
let fxList = generateApiUrl("fx/list")

/// 删除收货地址
let addressremove = generateApiUrl("address/remove")

/// 获取用户订单列表
let orderlist = generateApiUrl("order/list")

/// 创建订单/创建支付预付单
let ordersave = generateApiUrl("/order/save")

/// 获取订单详情
let orderitem = generateApiUrl("order/item")

/// 获取视频内容
let systemvideo = generateApiUrl("system/video")

/// 获取二维码
let systemqrCode = generateApiUrl("system/qrCode")

/// 获取个人银行卡信息(包括默认银行卡&账户可用于提现的余额)
let accountWithdrawDepositInfo = generateApiUrl("account/withdrawDepositInfo")

/// 删除银行卡
let bank_cardremove = generateApiUrl("bank_card/remove")

/// 提现
let bankwithdrawDeposit = generateApiUrl("bank_card/withdrawDeposit")

/// 忘记密码
let accountchangePassword = generateApiUrl("account/changePassword")
