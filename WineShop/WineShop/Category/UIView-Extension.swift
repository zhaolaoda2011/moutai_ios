//
//  UIView-Extension.swift
//  juzhuanwang
//
//  Created by zc on 2018/2/28.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    //返回该view所在VC
    func currentViewController() -> UIViewController? {
        for view in sequence(first: self.superview, next: { $0?.superview }) {
            if let responder = view?.next {
                if responder.isKind(of: UIViewController.self){
                    return responder as? UIViewController
                }
            }
        }
        return nil
    }
    
    //返回该view所在navigationcontroller
    func currentNavigationViewController() -> UINavigationController? {
        for view in sequence(first: self.superview, next: { $0?.superview }) {
            if let responder = view?.next {
                if responder.isKind(of: UINavigationController.self){
                    return responder as? UINavigationController
                }
            }
        }
        return nil
    }
}
