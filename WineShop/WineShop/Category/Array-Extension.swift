//
//  Array-Extension.swift
//  WineShop
//
//  Created by zc on 2018/5/18.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation

extension Array {
    subscript (safe index: Int) -> Element? {
        return (0..<count).contains(index) ? self[index] : nil
    }
}
