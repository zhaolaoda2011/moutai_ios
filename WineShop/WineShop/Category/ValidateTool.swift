//
//  ValidateTool.swift
//  juzhuanwang
//
//  Created by zc on 2018/2/26.
//  Copyright © 2018年 zj. All rights reserved.
//

import UIKit

enum Validate {
    case email(_: String)
    case phoneNum(_: String)
    case carNum(_: String)
    case username(_: String)
    case password(_: String)
    case nickname(_: String)
    case URL(_: String)
    case IP(_: String)
    
    var isRight: Bool {
        var predicateStr:String!
        var currObject:String!
        switch self {
        case let .email(str):
            predicateStr = "^([a-z0-9_\\.-]+)@([\\da-z\\.-]+)\\.([a-z\\.]{2,6})$"
            currObject = str
        case let .phoneNum(str):
            predicateStr = "1[3|4|5|7|8|][0-9]{9}"//"^((13[0-9])|(15[^4,\\D]) |(17[0,0-9])|(18[0,0-9]))\\d{8}$"
            currObject = str
        case let .carNum(str):
            predicateStr = "^[A-Za-z]{1}[A-Za-z_0-9]{5}$"
            currObject = str
        case let .username(str):
            predicateStr = "^[A-Za-z0-9]{6,20}+$"
            currObject = str
        case let .password(str):
            predicateStr = "^[a-zA-Z0-9]{6,20}+$"
            currObject = str
        case let .nickname(str):
            predicateStr = "^[\\u4e00-\\u9fa5]{4,8}$"
            currObject = str
        case let .URL(str):
            predicateStr = "^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$"
            currObject = str
        case let .IP(str):
            predicateStr = "^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
            currObject = str
        }
        let predicate =  NSPredicate(format: "SELF MATCHES %@" ,predicateStr)
        return predicate.evaluate(with: currObject)
    }
}

class ValidateTool: NSObject {
    ///是否是整数
    static func isPureInt(_ string:String)->Bool{
        let scan : Scanner = Scanner(string: string)
        
        var val:Int = 0
        
        return scan.scanInt(&val) && scan.isAtEnd
    }
    
    ///是否是正整数
    static func isPurePositiveInt(_ string:String)->Bool{
        let scan : Scanner = Scanner(string: string)
        
        var val:Int = 0
        
        let scanValue = scan.scanInt(&val) && scan.isAtEnd
        return  scanValue && val >= 0
    }
    
    ///是否是float
    static func isPurnFloat(_ string: String) -> Bool {
        
        let scan: Scanner = Scanner(string: string)
        
        var val:Float = 0
        
        return scan.scanFloat(&val) && scan.isAtEnd
        
    }
}

// MARK: 正则表达式
extension ValidateTool{
    
    //允许汉字或数字或字母或特殊字符 `~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？
    static func inputChineseOrNumbersLimit(_ string: String,_ max : Int,_ min : Int) -> Bool {
        let regex = "^[a-zA-Z0-9\u{4e00}-\u{9fa5}`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？\r\n\\s]{\(min),\(max)}+$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        let inputString = predicate.evaluate(with : string)
        return inputString
    }
    
//    //只能为中文
//    static func onlyInputChineseCharacters(_ string: String) -> Bool {
//        let inputString = "[\u{4e00}-\u{9fa5}]+"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", inputString)
//        let Chinese = predicate.evaluate(with: string)
//        return Chinese
//    }
//
//    //只能为数字
//    static func onlyInputTheNumber(_ string: String) -> Bool {
//        let numString = "[0-9]*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", numString)
//        let number = predicate.evaluate(with: string)
//        return number
//    }
//
//    //只能为小写
//    static func onlyInputLowercaseLetter(_ string: String) -> Bool {
//        let regex = "[a-z]*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        let letter = predicate.evaluate(with: string)
//        return letter
//    }
//
//
//    //只能为大写
//    static func onlyInputACapital(_ string: String) -> Bool {
//        let regex = "[A-Z]*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        let capital = predicate.evaluate(with : string)
//        return capital
//    }
//
//
//
//    //允许大小写
//    static func inputCapitalAndLowercaseLetter(_ string: String) -> Bool {
//        let regex = "[a-zA-Z]*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        let inputString = predicate.evaluate(with : string)
//        return inputString
//    }
//
//
//    //允许大小写或数字(不限字数)
//    static func inputLettersOrNumbers(_ string: String) -> Bool {
//        let regex = "[a-zA-Z0-9]*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        let inputString = predicate.evaluate(with : string)
//        return inputString
//    }
//
//
//
//    //允许大小写或数字(限字数)
//    static func inputNumberOrLetters(_ name: String,_ max : Int,_ min : Int) -> Bool {
//        let userNameRegex = "^[A-Za-z0-9]{\(min),\(max)}+$"
//        let userNamePredicate = NSPredicate(format: "SELF MATCHES %@", userNameRegex)
//        let inputString = userNamePredicate.evaluate(with : name)
//        return inputString
//    }
//
//    //允许汉字或数字(不限字数)
//    static func inputChineseOrNumbers(_ string: String) -> Bool {
//        let regex = "[\u{4e00}-\u{9fa5}][0-9]*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        let inputString = predicate.evaluate(with : string)
//        return inputString
//    }
//
//
//
//
//
//    //允许汉字，大小写或数字(不限字数)
//    static func inputChineseOrLettersAndNumbersNum(_ string: String) -> Bool {
//        let regex = "[\u{4e00}-\u{9fa5}]+[A-Za-z0-9]*"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        let inputString = predicate.evaluate(with : string)
//        return inputString
//    }
//
//
//
//    //允许汉字，大小写或数字(限字数)
//    static func inputChineseOrLettersNumberslimit(_ string: String) -> Bool {
//        let regex = "[\u{4e00}-\u{9fa5}]+[A-Za-z0-9]{6,20}+$"
//        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
//        let inputString = predicate.evaluate(with : string)
//        return inputString
//    }

}
