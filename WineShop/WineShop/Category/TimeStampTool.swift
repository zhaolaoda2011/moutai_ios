//
//  TimeStampTool.swift
//  juzhuanwang
//
//  Created by apple on 2018/2/18.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation

class TimeStampTool:NSObject{
    ///将时间戳转成对应格式的时间字符串  格式可以不用传
    static func convertTimeStampToDate(_ timeStamp : Int,_ format : String?)->String{
        //时间戳
//        let timeStamp = 1463637809
//        print("时间戳：\(timeStamp)")
        
        //转换为时间
        let timeInterval:TimeInterval = TimeInterval(timeStamp)
        let date = Date(timeIntervalSince1970: timeInterval)
        
        //格式话输出
        let dformatter = DateFormatter()
        dformatter.dateFormat = format == nil || (format?.isEmpty)! || format=="" ? "yyyy-MM-dd" : format //"yyyy年MM月dd日 HH:mm:ss"
        return dformatter.string(from: date)
    }
    
    /// 将时间转换成时间戳
    static func convertDateToTimeStamp(_ date : Date)->Int{
        let timeInterval:TimeInterval = date.timeIntervalSince1970
        let timeStamp = Int(timeInterval)
        return timeStamp
    }
    
    /// 将时间字符串转换成时间戳
    static func convertDateStringToTimeStamp(_ dateStr : String,_ format : String?)->Int{
        //格式话输出
        let dformatter = DateFormatter()
        dformatter.dateFormat = format == nil || (format?.isEmpty)! || format=="" ? "yyyy-MM-dd HH:mm:ss" : format //"yyyy年MM月dd日 HH:mm:ss"
        if let date = dformatter.date(from: dateStr){
            let timeInterval:TimeInterval = date.timeIntervalSince1970
            let timeStamp = Int(timeInterval)
            return timeStamp
        }
        
        return 0
    }
    
    ///将时间戳转成时间  格式可以不用传
    static func convertTimeStampToDate(_ timeStamp : Int)->Date{
        //时间戳
        //        let timeStamp = 1463637809
        //        print("时间戳：\(timeStamp)")
        
        //转换为时间
        let timeInterval:TimeInterval = TimeInterval(timeStamp)
        let date = Date(timeIntervalSince1970: timeInterval)
        
        return date
    }
    
    // 输入的时间和当前时间对比string
    static func earlyComparWithNowString(_ timeStr: String, _ format: String?) -> (Bool) {
        let comparTimeInterval : Int = convertDateStringToTimeStamp(timeStr, format)
        let now = Date()
        let nowTimeInterval : Int = convertDateToTimeStamp(now)
       return comparTimeInterval < nowTimeInterval
    }
    // 输入的时间和当前时间对比Int
    static func earlyComparWithNow(_ comparTimeInterval: Int) -> (Bool) {
        let now = Date()
        let nowTimeInterval : Int = convertDateToTimeStamp(now)
        return comparTimeInterval < nowTimeInterval
    }
    
    
    // 获取当前时间并转化成字符串
    static func getNowWithFormat(_ format : String? ) -> (String) {
        let nowDate  = Date()
        let dformatter = DateFormatter()
        dformatter.dateFormat = format ?? "yyyy-MM-dd HH:mm:ss"
        return dformatter.string(from: nowDate)
    }
}


