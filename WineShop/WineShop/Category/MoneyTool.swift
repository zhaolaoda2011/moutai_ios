//
//  MoneyTool.swift
//  juzhuanwang
//
//  Created by apple on 2018/2/18.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation

class MoneyTool: NSObject {
    static func convertMoneyToString(_ money : Int)->String{
        if money < 1000{
            return "\(money)"
        }else if money >= 1000 && money < 10000{
            return "\(money/1000)千"
        }else if money >= 10000{
            return "\(money/10000)万"
        }
        return "0"
    }
}
