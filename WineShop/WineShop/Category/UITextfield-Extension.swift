//
//  UITextfield-Extension.swift
//  juzhuanwang
//
//  Created by zc on 2018/3/16.
//  Copyright © 2018年 zj. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    /// 设置placeholder 的 字体颜色,字体大小,是否垂直居中
    func setPlaceHolder(placeHolder : String,font : UIFont,color : UIColor,isVerical : Bool){
        //------如果输入文字不居中，placeholder不居中，重写系统方法
        //-(CGRect)editingRectForBounds:(CGRect)bounds;
        //-(CGRect)placeholderRectForBounds:(CGRect)bounds;
        self.setPlaceHolderColor(placeHolder: placeHolder, color: color)
        self.setPlaceHolderFont(placeHolder: placeHolder, font: font, isVerical: isVerical)
    }
    
    /// 设置placeholder 的 字体颜色
    func setPlaceHolderColor(placeHolder : String,color : UIColor){
        self.placeholder = placeHolder
        self.setValue(color, forKeyPath: "_placeholderLabel.textColor")
    }
    
    /// 设置placeholder 的 字体大小  并且设置是否垂直居中
    func setPlaceHolderFont(placeHolder : String,font : UIFont,isVerical : Bool){
        if isVerical{
            let style = self.defaultTextAttributes[NSAttributedStringKey.paragraphStyle.rawValue] as! NSParagraphStyle
            let mutable = style.mutableCopy() as! NSMutableParagraphStyle
            mutable.minimumLineHeight = self.font!.lineHeight - (self.font!.lineHeight - font.lineHeight) / 2.0
            self.attributedPlaceholder = NSAttributedString(string: placeHolder, attributes: [NSAttributedStringKey.paragraphStyle : mutable,NSAttributedStringKey.font:font])
        }else{
            self.placeholder = placeHolder
            self.setValue(font, forKeyPath: "_placeholderLabel.font")
        }
    }
}
