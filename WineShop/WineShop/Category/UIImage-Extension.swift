//
//  UIImage-Extension.swift
//  Album
//
//  Created by zc on 2017/11/16.
//  Copyright © 2017年 zj. All rights reserved.
//

import Foundation
import UIKit

// MARK: 获取图片主要颜色
extension UIImage{
    ///获取图片中的某点的像素颜色值
    func getPixelColor(pos:CGPoint)->UIColor{
        if let cgimage = self.cgImage {
            if let provider = cgimage.dataProvider{
                if let pixelData = provider.data{
                    let data:UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
                    let pixelInfo: Int = ((Int(self.size.width) * Int(pos.y)) + Int(pos.x)) * 4
                    
                    let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
                    let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
                    let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
                    let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
                    return UIColor.init(red: r, green: g, blue: b, alpha: a)
                }
            }
        }
        return UIColor.withHex(hexInt: 0xf2f2f2)
    }
}

extension UIImage{
    convenience init?(gradientColors:[UIColor], size:CGSize = CGSize(width: 10, height: 10) )
    {
        UIGraphicsBeginImageContextWithOptions(size, true, 0)
        let context = UIGraphicsGetCurrentContext()
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colors = gradientColors.map {(color: UIColor) -> AnyObject? in return color.cgColor as AnyObject? } as NSArray
        let gradient = CGGradient(colorsSpace: colorSpace, colors: colors, locations: nil)
        // 第二个参数是起始位置，第三个参数是终止位置
        context!.drawLinearGradient(gradient!, start: CGPoint(x: 0, y: 0), end: CGPoint(x: size.width, y: 0), options: CGGradientDrawingOptions(rawValue: 0))

        self.init(cgImage:(UIGraphicsGetImageFromCurrentImageContext()?.cgImage!)!)
        UIGraphicsEndImageContext()
    }
}

// MARK: 颜色转UIImage
extension UIImage{
    class func createImage(color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image ?? nil
    }
}

// MARK: 文字转图片
extension UIImage{
    class func createFontImage(_ text:String,size:(CGFloat,CGFloat),backColor:UIColor=UIColor.orange,textColor:UIColor=UIColor.white,isCircle:Bool=true) -> UIImage?{
        // 过滤空""
        if text.isEmpty { return nil }
        // 取第一个字符(测试了,太长了的话,效果并不好)
        let letter = (text as NSString).substring(to: 1)
        let sise = CGSize(width: size.0, height: size.1)
        let rect = CGRect(origin: CGPoint.zero, size: sise)
        // 开启上下文
        UIGraphicsBeginImageContext(sise)
        // 拿到上下文
        guard let ctx = UIGraphicsGetCurrentContext() else { return nil }
        // 取较小的边
        let minSide = min(size.0, size.1)
        // 是否圆角裁剪
        if isCircle {
            UIBezierPath(roundedRect: rect, cornerRadius: minSide*0.5).addClip()
        }
        // 设置填充颜色
        ctx.setFillColor(backColor.cgColor)
        // 填充绘制
        ctx.fill(rect)
        let attr = [ NSAttributedStringKey.foregroundColor : textColor, NSAttributedStringKey.font : UIFont.systemFont(ofSize: minSide*0.5)]
        // 写入文字
        (letter as NSString).draw(at: CGPoint(x: minSide*0.25, y: minSide*0.25), withAttributes: attr)
        // 得到图片
        let image = UIGraphicsGetImageFromCurrentImageContext()
        // 关闭上下文
        UIGraphicsEndImageContext()
        return image
    }
}
