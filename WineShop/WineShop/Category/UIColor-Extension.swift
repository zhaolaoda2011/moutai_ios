//
//  UIColor-Extension.swift
//  Album
//
//  Created by zc on 2017/11/16.
//  Copyright © 2017年 zj. All rights reserved.
//

import Foundation
import UIKit

/// 获取颜色的方法
extension UIColor {
    /**
     获取颜色，通过16进制色值字符串，e.g. #ff0000， ff0000
     - parameter hexString  : 16进制字符串
     - parameter alpha      : 透明度，默认为1，不透明
     - returns: RGB
     */
    static func withHex(hexString hex: String, alpha:CGFloat = 1) -> UIColor {
        // 去除空格等
        var cString: String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
        // 去除#
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        // 必须为6位
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        // 红色的色值
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        // 字符串转换
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: alpha)
    }
    
    /**
     获取颜色，通过16进制数值
     - parameter hexInt : 16进制数值
     - parameter alpha  : 透明度
     - returns : 颜色
     */
    static func withHex(hexInt hex:Int32, alpha:CGFloat = 1) -> UIColor {
        let r = CGFloat((hex & 0xff0000) >> 16) / 255
        let g = CGFloat((hex & 0xff00) >> 8) / 255
        let b = CGFloat(hex & 0xff) / 255
        return UIColor(red: r, green: g, blue: b, alpha: alpha)
    }
    
    /**
     获取颜色，通过rgb
     - parameter red    : 红色
     - parameter green  : 绿色
     - parameter blue   : 蓝色
     - returns : 颜色
     */
    static func withRGB(_ red:CGFloat, _ green:CGFloat, _ blue:CGFloat) -> UIColor {
        return UIColor.withRGBA(red, green, blue, 1)
    }
    
    /**
     获取颜色，通过rgb
     - parameter red    : 红色
     - parameter green  : 绿色
     - parameter blue   : 蓝色
     - parameter alpha  : 透明度
     - returns : 颜色
     */
    static func withRGBA(_ red:CGFloat, _ green:CGFloat, _ blue:CGFloat, _ alpha:CGFloat) -> UIColor {
        return UIColor(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }
    
    //随机颜色
    static func withRandom() -> UIColor {
        let red = CGFloat(arc4random()%256)/255.0
        let green = CGFloat(arc4random()%256)/255.0
        let blue = CGFloat(arc4random()%256)/255.0
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    
    func toImage() -> UIImage? {
        return toImageWithSize(size: CGSize(width: 1, height: 1))
    }
    func toImageWithSize(size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        
        if let ctx = UIGraphicsGetCurrentContext() {
            let rectangle = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            ctx.setFillColor(self.cgColor)
            ctx.addRect(rectangle)
            ctx.drawPath(using: .fill)
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return colorImage
        } else {
            return nil
        }
    }
    //    func imageWithAlpha(alpha: CGFloat,size: CGSize) -> UIImage? {
    //        UIGraphicsBeginImageContextWithOptions(size, false, scale)
    //        draw(at: CGPoint.zero, blendMode: .normal, alpha: alpha)
    //        let newImage = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //        return newImage
    //    }
    
    static func getImageWithAlpha(alpha:CGFloat)->UIImage{
        let color = UIColor.withRGBA(255, 255,255, alpha)
        let colorSize =  CGSize.init(width: 1, height: 1)
        UIGraphicsBeginImageContext(colorSize);
        let context = UIGraphicsGetCurrentContext();
        context!.setFillColor(color.cgColor);
        context!.fill(CGRect.init(x: 0, y: 0, width: 1, height: 1));
        
        let img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return img!
    }
    
    ///获取rgba的值
    static func getRGBDictionaryByColor(color : UIColor)->[CGFloat]{
        var r : CGFloat = 0
        var g : CGFloat = 0
        var b : CGFloat = 0
        var a : CGFloat = 0
        if self.responds(to: #selector(getRed(_:green:blue:alpha:))){
            color.getRed(&r, green: &g, blue: &b, alpha: &a)
        }else{
            if let components = color.cgColor.components {
                r = components[0]
                g = components[1]
                b = components[2]
                a = components[3]
            }
        }
        return [r,g,b,a]
    }
    
    ///获取两个颜色之间的差值
    static func transColorBeginColor(beginColor : UIColor,endColor : UIColor)->[CGFloat]{
        let beginColorArr = UIColor.getRGBDictionaryByColor(color: beginColor)
        let endColorArr = UIColor.getRGBDictionaryByColor(color: endColor)
        return [endColorArr[0]-beginColorArr[0],endColorArr[1]-beginColorArr[1],endColorArr[2]-beginColorArr[2],endColorArr[3]-beginColorArr[3]]
    }
    
    ///根据过渡系数返回当前颜色
    static func getColorWithProgress(progress : CGFloat,beginColor : UIColor,endColor : UIColor)->UIColor{
        let beginColorArr = UIColor.getRGBDictionaryByColor(color: beginColor)
        let distanceArr = UIColor.transColorBeginColor(beginColor: beginColor, endColor: endColor)
        let r : CGFloat = beginColorArr[0] + progress * distanceArr[0]
        let g : CGFloat = beginColorArr[1] + progress * distanceArr[1]
        let b : CGFloat = beginColorArr[2] + progress * distanceArr[2]
        return UIColor.init(red: r, green: g, blue: b, alpha: 1)
    }
}
